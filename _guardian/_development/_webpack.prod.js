const path = require("path");
const merge = require("webpack-merge");
const common = require("./webpack.common.js");

module.exports = merge(common, {
    mode: "production",

    entry: {
        /** ENTRY- **/
        // "modules/filemonitor": path.join(__dirname, "./sources/modules/filemonitor/launcher.ts"),
        // "modules/mailer": path.join(__dirname, "./sources/modules/mailer/launcher.ts"),
        "programs/webhandlers/sys-id/sys-id": path.join(__dirname, "./sources/programs/webhandlers/sys-id/main.ts"),
        /** -ENTRY **/
    },

    optimization: {
        minimize: true,
    },

});