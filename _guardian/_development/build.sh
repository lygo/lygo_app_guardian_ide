#cd _typescript || exit
echo "BUILDING..."
#npm run dev
npm run build

echo "CLEAR DESTINATION FOLDER...."
#rm -rfv ../programs/bundle/*
echo "COPY CONTENT"
cp -R build/programs/. ../programs
echo "DONE!"