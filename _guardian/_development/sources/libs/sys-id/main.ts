class Main {

    constructor() {

    }

    /**
     * Returns unique machine ID.
     * @param opt_filename Optional file name. Default is: "serial.txt"
     * @returns {string|undefined}
     */
    public get(opt_filename?: string): string {

        const sys = use("sys");
        const path = use("path");
        const fs = use("fs");

        const root: string = runtime.context["_dirname"];
        // console.log("SysId.get#context", JSON.stringify(runtime.context));

        try {
            opt_filename = opt_filename || "serial.txt";
            let id = sys.id();
            const filename = path.resolve(root, opt_filename);
            console.log("SysId.get", id, filename);

            if (fs.existsSync(filename)) {
                // read the file
                const txt = fs.readFileSync(filename);
                if (!!txt) {
                    id = txt.trim();
                } else {
                    // creates the file
                    fs.writeFileSync(filename, id);
                }
            } else {
                // creates the file
                fs.writeFileSync(filename, id);
            }


            return id; // send id
        } catch (err) {
            console.error("sys-id.js", err);
        }
        return ""; //  nothing back
    }
}

// -------------------------------------------------------------------------
// p r i v a t e
// -------------------------------------------------------------------------

// -------------------------------------------------------------------------
// e x p o r t s
// -------------------------------------------------------------------------
let instance: Main;

function getInstance(): Main {
    if (null == instance) {
        instance = new Main();
    }
    return instance;
}

export default getInstance();