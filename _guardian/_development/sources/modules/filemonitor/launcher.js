import guardian from "../../guardian";

const path = guardian.require("path");
const fs = guardian.require("fs");
const linereader = guardian.require("line-reader");

// -------------------------------------------------------------------------
// e x p o r t s
// -------------------------------------------------------------------------

module.exports = {

    isFileGreaterThan: function (file, sizeMb) {
        var filename = path.resolve(file);
        if (fs.existsSync(filename)) {
            var bytes = fs.statSync(filename).size;
            var mbytes = bytes / (1024 * 1024);
            // console.debug("filemonitor.js", "SIZE", bytes + "b", mbytes + "Mb");
            return mbytes > sizeMb;
        } else {
            throw "File not found: " + filename
        }
    },

    readLines: function (file, count) {
        const filename = path.resolve(file);
        if (fs.existsSync(filename)) {
            return linereader.readLines(filename, count);
        } else {
            throw "File not found: " + filename
        }
    },

    writeFile: function (file, text) {
        const filename = path.resolve(file);
        fs.writeFileSync(filename, text);
    },

    removeFile: function (file) {
        const filename = path.resolve(file);
        if (fs.existsSync(filename)) {
            fs.unlinkSync(filename);
        }
    }
}