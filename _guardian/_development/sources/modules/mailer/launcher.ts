// -------------------------------------------------------------------------
// e x p o r t s
// -------------------------------------------------------------------------

module.exports = {

    send: function (subject:String, text:String, from:String, to:String, attachments:Array<any>, callback:Function) {
        const nodemailer = require("nodemailer");
        const path = require("path");
        const fs = require("fs");

        const settings = {
            host: 'smtp.mailtrap.io',
            port: 2525,
            secure: false,
            auth: {
                user: '8590e66d494168',
                pass: '8926a5d72a274f'
            }
        };

        var transport = nodemailer.createTransport(settings);

        var message:any = {
            from: from, // Sender address '9f48ff8df3-36d93f@inbox.mailtrap.io'
            to: to,     // List of recipients 'angelo.geminiani@gmail.com',
            subject: subject, // Subject line
            text: text,
            // attachments: [{"filename": path.basename(filename), "path": filename}]
        };

        if (!!attachments && attachments.length > 0) {
            message.attachments = [];
            attachments.forEach(function (filename, index) {
                if (fs.existsSync(filename)) {
                    message.attachments.push({"filename": path.basename(filename), "path": filename});
                }
            });
        }
        transport.sendMail(message, callback);
    }

}