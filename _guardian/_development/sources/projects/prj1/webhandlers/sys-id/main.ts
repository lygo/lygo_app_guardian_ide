import sysid from "../../../../libs/sys-id/main"

const id = sysid.get();

/**
 * Sample using "res" object
 */
const res = runtime.context["res"];
res.json({"response": id});
res.status(200);

/**
 * Sample writing directly response to context
 */
//runtime.context["response"] = id;