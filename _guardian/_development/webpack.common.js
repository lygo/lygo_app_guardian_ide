
const path = require("path");

module.exports = {
    entry: {},
    output: {
        path: path.join(__dirname, "build"),
        filename: (context) => {
            return "[name].js";
        }
    },
    plugins: [],
    module: {
        rules: [
            {
                exclude: /node_modules/,
                use: "ts-loader"
            },
        ]
    },
    resolve: {
        extensions: [".ts", ".js"]
    },
};
