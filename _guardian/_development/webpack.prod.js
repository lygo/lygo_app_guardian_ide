const path = require("path");
const merge = require("webpack-merge");
const common = require("./webpack.common.js");

module.exports = merge(common, {
    mode: "production",
    entry: {
"programs/8thsense/webhandlers/echo/echo":path.join(__dirname, "./sources/projects/8thsense/webhandlers/echo/main.ts"),
"programs/pap24-agent/webhandlers/api-generate-qrcode/api-generate-qrcode":path.join(__dirname, "./sources/projects/pap24-agent/webhandlers/api-generate-qrcode/main.ts"),
"programs/pap24-agent/webhandlers/api-generate-uid/api-generate-uid":path.join(__dirname, "./sources/projects/pap24-agent/webhandlers/api-generate-uid/main.ts"),
"programs/pap24-agent/webhandlers/api-resend-email/api-resend-email":path.join(__dirname, "./sources/projects/pap24-agent/webhandlers/api-resend-email/main.ts"),
"programs/pap24-agent/webhandlers/api-submit-uid/api-submit-uid":path.join(__dirname, "./sources/projects/pap24-agent/webhandlers/api-submit-uid/main.ts"),
"programs/pap24-agent/webhandlers/test/test":path.join(__dirname, "./sources/projects/pap24-agent/webhandlers/test/main.ts"),
"programs/pap24-server/tasks/nio-srv/nio-srv":path.join(__dirname, "./sources/projects/pap24-server/tasks/nio-srv/main.ts"),
"programs/pap24-server/webhandlers/echo/echo":path.join(__dirname, "./sources/projects/pap24-server/webhandlers/echo/main.ts"),
"programs/pap24-server/webhandlers/test/test":path.join(__dirname, "./sources/projects/pap24-server/webhandlers/test/main.ts"),
"programs/prj1/tasks/simple/simple":path.join(__dirname, "./sources/projects/prj1/tasks/simple/main.ts"),
"programs/prj1/webhandlers/sys-id/sys-id":path.join(__dirname, "./sources/projects/prj1/webhandlers/sys-id/main.ts"),
"programs/webserver/tasks/init/init":path.join(__dirname, "./sources/projects/webserver/tasks/init/main.ts")
},
    optimization: {
        minimize: true,
    },
});