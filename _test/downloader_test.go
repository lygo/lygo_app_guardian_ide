package _test

import (
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/downloader"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"fmt"
	"testing"
)

func TestVersion(t *testing.T) {

	dir := lygo_paths.Absolute("./download")
	lygo_paths.Mkdir(dir + lygo_paths.OS_PATH_SEPARATOR)
	d  := downloader.NewDownloader(dir, dir, dir)
	fmt.Println( d.LatestRuntimeVersion() )
}

func TestAssertRuntimeLatest(t *testing.T) {

	dir := lygo_paths.Absolute("./download")
	lygo_paths.Mkdir(dir + lygo_paths.OS_PATH_SEPARATOR)
	d  := downloader.NewDownloader(dir, dir, dir)
	err := d.AssertRuntimeLatest()
	if nil!=err{
		t.Error(err)
		t.FailNow()
	}
}

func TestDownloader(t *testing.T) {

	dir := lygo_paths.Absolute("./download")
	lygo_paths.Mkdir(dir + lygo_paths.OS_PATH_SEPARATOR)
	d  := downloader.NewDownloader(dir, dir, dir)
	err := d.AssertRuntimeExists()
	if nil!=err{
		t.Error(err)
		t.FailNow()
	}
}

func TestDownloadWebUI(t *testing.T) {

	dir := lygo_paths.Absolute("./download")
	lygo_paths.Mkdir(dir + lygo_paths.OS_PATH_SEPARATOR)
	d  := downloader.NewDownloader(dir, dir, dir)
	err := d.DownloadWebUI()
	if nil!=err{
		t.Error(err)
		t.FailNow()
	}
}
