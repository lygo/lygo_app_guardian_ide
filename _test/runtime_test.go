package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_exec"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"fmt"
	"os"
	"os/exec"
	"testing"
	"time"
)

func TestRuntimeDirect(t *testing.T) {

	cmd := lygo_paths.Absolute("../guardian")
	dir := lygo_paths.Dir(cmd)
	fmt.Println("Running: " + cmd)
	dirWork := lygo_paths.Concat(dir, "_guardian")

	//args := fmt.Sprintf("run -m=debug -dir_work=%v -dir_app=%v", dirWork, dir)
	c := exec.Command(cmd, "run", "-m", "debug", "-dir_work", dirWork)
	c.Stdout = os.Stdout
	c.Stderr = os.Stderr
	err := c.Start()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	time.Sleep(5 * time.Second)
}

func TestRuntime(t *testing.T) {

	cmd := lygo_paths.Absolute("../guardian")
	dir := lygo_paths.Dir(cmd)
	fmt.Println("Running: " + cmd)
	dirWork := lygo_paths.Concat(dir, "_guardian")

	//args := fmt.Sprintf("run -m=debug -dir_work=%v -dir_app=%v", dirWork, dir)
	executor := lygo_exec.NewExecutorWithDir(cmd, dir)
	err := executor.Run("run", "-m", "debug", "-dir_work", dirWork, "-dir_app", dir)

	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	time.Sleep(10 * time.Second)
}
