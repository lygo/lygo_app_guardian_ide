# ---------------- BUILD AND DEPLOY UI ---------------
echo "BUILDING...."
npm run build
echo "BUILDING DONE!"

echo "COPYING FILES TO TARGET...."
cp -R build/. ../_guardian/_development/_webui/www
echo "DONE!"

echo "ZIP FILES...."
zip -r a-webui.zip ./build
echo "COPYING ZIP FILE"
cp a-webui.zip ../_build/www/a-webui.zip
echo "REMOVING ZIP FILE"
rm a-webui.zip
echo "DONE!"
