import React, {Component} from 'react';
import {HashRouter, Route, Switch,} from "react-router-dom";
import {Redirect} from "react-router";
import {Col, Layout, Row} from 'antd';
import NavAppSider from "./components/nav-app-sider/NavAppSider";
import PageProjects from "./pages/projects/PageProjects";
import PageRuntime from "./pages/runtime/PageRuntime";
import PageLogs from "./pages/logs/PageLogs";
import constants from "./commons/constants";
import 'antd/dist/antd.css';
import './App.css';
import PageProjectsUI from "./pages/projects_ui/PageProjectsUI";


// ----------------------------------------------------
// app
// ----------------------------------------------------

const {Header, Content, Footer} = Layout;

class App extends Component {

    state = {
        activePage: "",
        version: "v0.0.0",
    }

    constructor(props) {
        super(props);
        this.state.activePage = window.location.hash || "#/projects";
    }

    componentDidMount() {
        const endpoint = "/dev-api/v1/utils/version";
        constants.API.get(endpoint)
            .then(value => {
                if (!!value && !!value.data && !!value.data.response) {
                    this.setState({version: value.data.response});
                } else {
                    console.warn(endpoint + "response:", value);
                }
            }).catch(reason => console.error(reason))
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        // console.log("APP UPDATE", prevState, snapshot);
    }

    handleSelect = (eventKey) => {
        const key = eventKey.key;
        // console.log(eventKey);
        this.setState({activePage: key})
    }
    handleQuit = () => {
        // close application
        constants.API.get("/quit").catch(err => console.error(err)).then(() => console.log("bye bye!"));
    }

    render() {

        return <HashRouter>
            <Layout className={"app"}>
                <NavAppSider version={this.state.version} active={this.state.activePage} onSelect={this.handleSelect}/>
                <Layout className="site-layout">
                    <Header className="" style={{padding: 0}}>
                        <Row>
                            <Col span={6}> </Col>
                            <Col span={6}> </Col>
                            <Col span={6}> </Col>
                            <Col span={6}>
                                <div style={{
                                    color: "#fff",
                                    textAlign: "right",
                                    paddingRight: 10
                                }}>{this.state.version}</div>
                            </Col>
                        </Row>
                    </Header>
                    <Content className={"site-content"}>
                        <div>
                            <Switch>
                                <Route exact path="/">
                                    <Redirect to="/projects"/>
                                </Route>
                                <Route path={["/projects"]}>
                                    <PageProjects/>
                                </Route>
                                <Route path={["/projects_ui"]}>
                                    <PageProjectsUI/>
                                </Route>
                                <Route path="/runtime">
                                    <PageRuntime/>
                                </Route>
                                <Route path="/logs">
                                    <PageLogs/>
                                </Route>
                            </Switch>
                        </div>
                    </Content>
                    <Footer className={"site-footer"}>GUARDIAN ©2020 Created by Gian Angelo Geminiani</Footer>
                </Layout>
            </Layout>
        </HashRouter>
    }


}

export default App;
