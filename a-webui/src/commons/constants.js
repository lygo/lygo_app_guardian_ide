import axios from "axios";

const constants = {
    APP_NAME: "GUARDIAN GUI",
    APP_VERSION: "v1.0.0",

    API: axios.create({
        baseURL: "http://localhost:9090/", //  + window.location.host,
        responseType: 'json',
        headers: {
            "Authorization":"Basic MTIzOmFiYw==",
        },
    })
}

export default constants;