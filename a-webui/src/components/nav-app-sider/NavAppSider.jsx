import React, {Component} from "react";
import {Avatar, Layout, Menu, Typography} from "antd";
import {CodeOutlined, FileOutlined, ProjectOutlined} from "@ant-design/icons";
import "./NavAppSider.css";
import icon from "../../icon.png";

const {Sider} = Layout;
// const {Header, Content, Footer} = Layout;
const {Item} = Menu;
const {Title} = Typography;

class NavAppSider extends Component {

    state = {
        collapsed: false,
    }

    constructor(props) {
        super(props);
        this.state.collapsed = false;
    }

    onCollapse = collapsed => {
        // console.log(collapsed);
        this.setState({collapsed});
    };

    render() {
        const {collapsed} = this.state;
        const selected = [this.props.active || '#/projects'];

        return <Sider collapsible collapsed={collapsed} onCollapse={this.onCollapse}>
            <div className="logo">
                <Title level={3} className={this.state.collapsed ? "hidden" : ""}>GUARDIAN</Title>
                <Avatar size={48} src={icon} className={this.state.collapsed ? "" : "hidden"}/>
            </div>
            <Menu onSelect={this.props.onSelect} theme="dark" defaultSelectedKeys={['#/home']} selectedKeys={selected}
                  mode="inline">
                <Item key="#/projects" icon={<ProjectOutlined />}>
                    <a href={'#/projects'}>BackEnd Projects</a>
                </Item>
                <Item key="#/projects_ui" icon={<ProjectOutlined />}>
                    <a href={'#/projects_ui'}>FrontEnd Projects</a>
                </Item>
                <Item key="#/runtime" icon={<CodeOutlined/>}>
                    <a href={'#/runtime'}>Runtime</a>
                </Item>
                <Item key="#/logs" icon={<FileOutlined/>}>
                    <a href={'#/logs'}>IDE Logs</a>
                </Item>
                <Item key="9" icon={<FileOutlined/>}>
                    Files
                </Item>

            </Menu>
        </Sider>
    }
}

export default NavAppSider;