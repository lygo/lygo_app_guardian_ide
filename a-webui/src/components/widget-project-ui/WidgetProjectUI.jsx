import {Component} from "react";
import {Spin, Switch, Typography} from "antd";
import constants from "../../commons/constants";
import {FolderOpenOutlined, FormOutlined, PlusOutlined} from "@ant-design/icons";
import "./WidgetProjectUI.css";

const timeout = 2 * 1000;

class WidgetProjectUI extends Component {

    state = {
        ready: false,
        data: "",
    }

    constructor(props) {
        super(props);
        this.timer = undefined;
    }

    componentDidMount() {
        //console.log("PageNodes mounted.");
        this.doQuery();
    }

    componentWillUnmount() {
        this.disableTimer();
    }

    disableTimer() {
        if (!!this.timer) {
            clearInterval(this.timer);
            this.timer = undefined;
        }
    }

    enableTimer() {
        this.disableTimer();
        this.timer = setInterval(this.onTimer.bind(this), timeout);
    }

    onTimer() {
        this.doQuery();
    }

    doQuery() {
        const name = this.props.name;
        if (name === "+") {
            this.setState({
                ready: true,
            })
            return;
        }
        this.disableTimer();
        const url = "/dev-api/v1/files/get_project_ui_info?name=" + name;
        constants.API.get(url)
            .catch(reason => console.error(url, reason))
            .then(value => {
                if (!!value) {
                    const data = value.data;
                    if (!!data) {
                        if (!!data.error) {
                            console.error(url, data.error);
                        } else if (!!data.response) {
                            this.setState({
                                ready: true,
                                data: data.response,
                            })
                        }
                    }
                }
                this.enableTimer();
            });
    }

    isActive(data) {
        if (!!data && !!data.info) {
            return !!data.info.active;
        }
        return false
    }

    onChangeActive(checked) {
        // console.log("ACTIVE:",checked)
        if (checked) {
            this.disableTimer();
            const name = this.props.name;
            const url = "/dev-api/v1/files/activate_project_ui?name=" + name;
            constants.API.get(url)
                .catch(reason => console.error(url, reason))
                .then(value => {
                    if (!!value) {
                        const data = value.data;
                        if (!!data) {
                            if (!!data.error) {
                                console.error(url, data.error);
                            } else if (!!data.response) {
                                this.doQuery();
                            }
                        }
                    }
                });
        }
    }

    onOpenDir() {
        const name = this.props.name;
        const url = "/dev-api/v1/files/open_project_ui_dir?name=" + name;
        constants.API.get(url)
            .catch(reason => console.error(url, reason))
            .then(value => {
            });
    }

    onOpenVSCode() {
        const name = this.props.name;
        const url = "/dev-api/v1/files/open_project_ui_vscode?name=" + name;
        constants.API.get(url)
            .catch(reason => console.error(url, reason))
            .then(value => {
            });
    }

    render() {
        const name = this.props.name;
        const data = this.state.data;
        const programs = data.programs || [];
        const is_plus = name === "+";
        const programsNames = [];
        programs.forEach(program => {
            programsNames.push(program.name);
        });
        let widget;
        if (is_plus) {
            widget = <div style={{width: "100%", textAlign: "center"}}>
                <PlusOutlined className={this.props.loading?"hidden":""} onClick={this.props.onAddProject} style={{fontSize: "xxx-large"}}/>
                <Spin className={this.props.loading?"":"hidden"}/>
            </div>
        } else {
            widget = <div style={{width: "100%"}}>
                <div style={{display: "flex", flexDirection: "row", width: "100%"}}>
                    <Typography.Title level={5} style={{
                        textOverflow: "ellipsis",
                        whiteSpace: "nowrap"
                    }}>{this.props.name}</Typography.Title>
                    <div style={{width: "100%", textAlign: "right", fontSize: "large"}}>
                        <FolderOpenOutlined style={{marginRight: 10}} onClick={this.onOpenDir.bind(this)}/>
                        <FormOutlined style={{marginRight: 10}} onClick={this.onOpenVSCode.bind(this)}/>
                    </div>
                </div>
                <div style={{padding: 5}}>
                    <div>
                        <span style={{marginRight: 10}}>Active:</span>
                        <Switch checked={this.isActive(data)} onChange={this.onChangeActive.bind(this)}/>
                    </div>
                    <div style={{marginTop: 10}}>
                        <strong>Programs:</strong><span>{programs.length + " (" + programsNames + ")"}</span>

                    </div>
                </div>
            </div>
        }
        return <div className={is_plus ? "widget-prj-plus shadowed" : "widget-prj shadowed"}>
            <Spin className={this.state.ready ? "hidden" : ""}/>
            <div className={this.state.ready ? "" : "hidden"} style={{width: "100%"}}>
                {widget}
            </div>
        </div>
    }
}

export default WidgetProjectUI;