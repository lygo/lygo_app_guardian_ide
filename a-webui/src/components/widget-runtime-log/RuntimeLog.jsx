import {Component} from "react";
import {Spin} from "antd";
import constants from "../../commons/constants";
import "./RuntimeLog.css";
import {DeleteOutlined, FileOutlined, FolderOpenOutlined} from "@ant-design/icons";

const timeout = 3 * 1000;

class RuntimeLog extends Component {

    state = {
        ready: false,
        data: "",
    }

    constructor(props) {
        super(props);
        console.debug(props);
        this.timer = undefined;
    }

    componentDidMount() {
        //console.log("PageNodes mounted.");
        this.doQuery();
        this.enableTimer();
    }

    componentWillUnmount() {
        this.disableTimer();
    }

    disableTimer() {
        if (!!this.timer) {
            clearInterval(this.timer);
            this.timer = undefined;
        }
    }

    enableTimer() {
        this.disableTimer();
        this.timer = setInterval(this.onTimer.bind(this), timeout);
    }

    onTimer() {
        this.doQuery();
    }

    doQuery(queryParams) {
        queryParams = queryParams || {
            filename: this.props.filename || "logging.log",
        };
        const url = "/dev-api/v1/" + (this.props.type || "runtime") + "/get_log";
        console.log(url);
        constants.API.get(url, queryParams)
            .catch(reason => console.error(url, reason))
            .then(value => {
                if (!!value) {
                    const data = value.data;
                    if (!!data) {
                        if (!!data.error) {
                            console.error(url, data.error);
                            this.setState({
                                ready: true,
                                data: data.error,
                            })
                        } else {
                            this.setState({
                                ready: true,
                                data: data.response,
                            })
                        }
                    }
                }
            });
    }

    onOpenDir() {
        const url = "/dev-api/v1/" + (this.props.type || "runtime") + "/open_log_dir";
        constants.API.get(url)
            .catch(reason => console.error(url, reason))
            .then(value => {
                if (!!value) {
                    const data = value.data;
                    if (!!data) {
                        if (!!data.error) {
                            console.error(url, data.error);
                            this.setState({
                                ready: true,
                                data: data.error,
                            })
                        } else {
                            this.setState({
                                ready: true
                            })
                        }
                    }
                }
            });
    }

    onOpenFile() {
        const url = "/dev-api/v1/" + (this.props.type || "runtime") + "/open_log_file";
        constants.API.get(url)
            .catch(reason => console.error(url, reason))
            .then(value => {
                if (!!value) {
                    const data = value.data;
                    if (!!data) {
                        if (!!data.error) {
                            console.error(url, data.error);
                            this.setState({
                                ready: true,
                                data: data.error,
                            })
                        } else {
                            this.setState({
                                ready: true
                            })
                        }
                    }
                }
            });
    }

    onResetFile() {
        const url = "/dev-api/v1/" + (this.props.type || "runtime") + "/clear_log_file";
        constants.API.get(url)
            .catch(reason => console.error(url, reason))
            .then(value => {
                if (!!value) {
                    const data = value.data;
                    if (!!data) {
                        if (!!data.error) {
                            console.error(url, data.error);
                            this.setState({
                                ready: true,
                                data: data.error,
                            })
                        } else {
                            this.doQuery();
                        }
                    }
                }
            });
    }

    render() {
        const type = !!this.props.type ? this.props.type : "runtime";
        const text_height = !!this.props.heightVH ? this.props.heightVH + "vh" : "48vh";
        const text = this.state.data;
        const lines = text.split("\n");
        let count = 0;
        return <div className="shadowed runtime-log">
            <Spin className={this.state.ready ? "hidden" : ""}/>
            <div className={this.state.ready ? "runtime-log-head" : "hidden"}>
                <div className={"runtime-log-head"} style={{display: "flex", flexDirection: "row", width: "100%"}}>
                    <div>{type.toUpperCase()} LOGGING:</div>
                    <div style={{width: "100%", textAlign: "right", fontSize: "large"}}>
                        <FolderOpenOutlined style={{marginRight: 10}} onClick={this.onOpenDir.bind(this)}/>
                        <FileOutlined style={{marginRight: 10}} onClick={this.onOpenFile.bind(this)}/>
                        <DeleteOutlined onClick={this.onResetFile.bind(this)}/>
                    </div>
                </div>
                <div className={"runtime-log-text"} style={{height: text_height}}>
                    {
                        lines.map(line => {
                            return <div key={count++}>{line}</div>
                        })
                    }
                </div>
            </div>
        </div>

    }
}

export default RuntimeLog;