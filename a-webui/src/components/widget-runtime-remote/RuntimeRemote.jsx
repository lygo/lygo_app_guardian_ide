import {Component} from "react";
import {Button, Spin, Statistic} from "antd";
import constants from "../../commons/constants";
import {DownloadOutlined} from "@ant-design/icons";

const timeout = 3 * 1000;

class RuntimeRemote extends Component {

    state = {
        ready: false,
        remote: "0",
        local: "0",
        loading: false,
    }

    constructor(props) {
        super(props);
        console.debug(props);
        this.timer = undefined;
    }

    componentDidMount() {
        //console.log("PageNodes mounted.");
        this.doQuery();
        this.doQueryLocal();
        this.enableTimer();
    }

    componentWillUnmount() {
        this.disableTimer();
    }

    disableTimer() {
        if (!!this.timer) {
            console.debug("RuntimeRemote", "disableTimer");
            clearInterval(this.timer);
            this.timer = undefined;
        }
    }

    enableTimer() {
        this.disableTimer();
        this.timer = setInterval(this.onTimer.bind(this), timeout);
    }

    onTimer() {
        this.doQuery();
        this.doQueryLocal();
    }

    doQuery(queryParams) {
        queryParams = queryParams || {};
        const url = "/dev-api/v1/runtime/remote_version";
        constants.API.get(url, queryParams)
            .catch(reason => console.error(url, reason))
            .then(value => {
                if (!!value) {
                    const data = value.data;
                    if (!!data) {
                        if (!!data.error) {
                            console.error(url, data.error);
                            this.setState({
                                ready: true,
                                remote: data.error,
                            })
                        } else if (!!data.response) {
                            this.setState({
                                ready: true,
                                remote: data.response,
                            })
                        }
                    }
                }
            });
    }

    doQueryLocal() {
        const url = "/dev-api/v1/runtime/version";
        constants.API.get(url)
            .catch(reason => console.error(url, reason))
            .then(value => {
                if (!!value) {
                    const data = value.data;
                    if (!!data) {
                        if (!!data.error) {
                            console.error(url, data.error);
                            this.setState({
                                ready: true,
                                local: data.response,
                            })
                        } else if (!!data.response) {
                            this.setState({
                                ready: true,
                                local: data.response,
                            })
                        }
                    }
                }
            });
    }

    onDownload() {
        this.setState({loading: true});
        const url = "/dev-api/v1/runtime/remote_download";
        constants.API.get(url)
            .catch(reason => console.error(url, reason))
            .then(value => {
                this.setState({loading: false});
                if (!!value) {
                    const data = value.data;
                    if (!!data) {
                        if (!!data.error) {
                            console.error(url, data.error);
                            this.setState({
                                ready: true,
                                remote: data.error,
                            })
                        } else if (!!data.response) {
                            this.setState({
                                ready: true,
                                remote: data.response,
                            })
                        }
                    }
                }
            });
    }

    render() {
        const remote = this.state.remote || "0";
        const local = this.state.local || "0";
        return <div className="widget shadowed">
            <Spin className={this.state.ready ? "hidden" : ""}/>
            <div className={this.state.ready ? "" : "hidden"}>
                <div style={{display: "flex"}}>
                    <Statistic title="Local " value={local} style={{marginRight: 15}}/>
                    <Statistic title="Remote " value={remote}/>
                </div>

                <Button icon={<DownloadOutlined/>} shape="round" size="large" loading={this.state.loading}
                        style={{marginTop: 16}} type="primary" onClick={this.onDownload.bind(this)}>
                    Download
                </Button>
            </div>

        </div>

    }
}

export default RuntimeRemote;