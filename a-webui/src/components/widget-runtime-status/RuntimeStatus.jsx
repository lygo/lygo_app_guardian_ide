import {Component} from "react";
import {Button, Spin, Statistic} from "antd";
import constants from "../../commons/constants";
import {PoweroffOutlined} from "@ant-design/icons";

const timeout = 3 * 1000;

class RuntimeStatus extends Component {

    state = {
        ready: false,
        data: "false",
        loading: false,
    }

    constructor(props) {
        super(props);
        console.debug(props);
        this.timer = undefined;
    }

    componentDidMount() {
        //console.log("PageNodes mounted.");
        this.doQuery();
        this.enableTimer();
    }

    componentWillUnmount() {
        this.disableTimer();
    }

    disableTimer() {
        if (!!this.timer) {
            clearInterval(this.timer);
            this.timer = undefined;
        }
    }

    enableTimer() {
        this.disableTimer();
        this.timer = setInterval(this.onTimer.bind(this), timeout);
    }

    onTimer() {
        this.doQuery();
    }

    doQuery(queryParams) {
        queryParams = queryParams || {};
        const url = "/dev-api/v1/runtime/is_running";
        constants.API.get(url, queryParams)
            .catch(reason => console.error(url, reason))
            .then(value => {
                if (!!value) {
                    const data = value.data;
                    if (!!data) {
                        if (!!data.error) {
                            console.error(url, data.error);
                            this.setState({
                                ready: true,
                                data: "false",
                            })
                        } else if (!!data.response) {
                            this.setState({
                                ready: true,
                                data: data.response,
                            })
                        }
                    }
                }
            });
    }

    onStop() {
        this.setState({loading: true});
        const url = "/dev-api/v1/runtime/stop";
        constants.API.get(url)
            .catch(reason => console.error(url, reason))
            .then(value => {
                this.setState({loading: false});
                if (!!value) {
                    const data = value.data;
                    if (!!data) {
                        if (!!data.error) {
                            console.error(url, data.error);
                            this.setState({
                                ready: true,
                                data: "false",
                            })
                        } else if (!!data.response) {
                            this.setState({
                                ready: true,
                                data: data.response,
                            })
                        }
                    }
                }
            });
    }

    onStart() {
        this.setState({loading: true});
        const url = "/dev-api/v1/runtime/start";
        constants.API.get(url)
            .catch(reason => console.error(url, reason))
            .then(value => {
                this.setState({loading: false});
                if (!!value) {
                    const data = value.data;
                    if (!!data) {
                        if (!!data.error) {
                            console.error(url, data.error);
                            this.setState({
                                ready: true,
                                data: "false",
                            })
                        } else if (!!data.response) {
                            this.setState({
                                ready: true,
                                data: data.response,
                            })
                        }
                    }
                }
            });
    }

    render() {
        const running = this.state.data === "true";
        const value = running ? "RUNNING" : "STOPPED";
        return <div className="widget shadowed">
            <Spin className={this.state.ready ? "hidden" : ""}/>
            <div className={this.state.ready ? "" : "hidden"}>
                <Statistic title="Runtime Status" value={value}/>
                <Button icon={<PoweroffOutlined/>} shape="round" size="large" danger loading={this.state.loading}
                        className={running ? "" : "hidden"} style={{marginTop: 16}} type="primary"
                        onClick={this.onStop.bind(this)}>
                    Stop
                </Button>
                <Button icon={<PoweroffOutlined/>} shape="round" size="large" loading={this.state.loading}
                        className={running ? "hidden" : ""} style={{marginTop: 16}} type="primary"
                        onClick={this.onStart.bind(this)}>
                    Start
                </Button>
            </div>

        </div>

    }
}

export default RuntimeStatus;