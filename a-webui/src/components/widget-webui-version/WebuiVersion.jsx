import {Component} from "react";
import {Button, Spin, Statistic} from "antd";
import constants from "../../commons/constants";
import {DownloadOutlined} from "@ant-design/icons";

class WebuiVersion extends Component {

    state = {
        ready: false,
        loading: false,
        data: "",
    }

    constructor(props) {
        super(props);
        console.debug(props);
    }

    componentDidMount() {
        //console.log("PageNodes mounted.");
        this.doQuery();
    }

    doQuery(queryParams) {
        const url = "/dev-api/v1/webui/version";
        constants.API.get(url)
            .catch(reason => console.error(url, reason))
            .then(value => {
                if (!!value) {
                    const data = value.data;
                    if (!!data) {
                        if (!!data.error) {
                            console.error(url, data.error);
                        } else if (!!data.response) {
                            this.setState({
                                ready: true,
                                data: data.response,
                            })
                        }
                    }
                }
            });
    }

    onDownload() {
        this.setState({loading: true});
        const url = "/dev-api/v1/webui/remote_download";
        constants.API.get(url)
            .catch(reason => console.error(url, reason))
            .then(value => {
                let response = "";
                if (!!value) {
                    const data = value.data;
                    if (!!data) {
                        if (!!data.error) {
                            console.error(url, data.error);
                            response = data.error;
                        } else if (!!data.response) {
                            response = data.response;
                        }
                    }
                }
                this.setState({
                    ready: true,
                    loading: false,
                    data: response,
                })
            });
    }

    render() {
        const version = this.state.data;
        return <div className="widget shadowed">
            <Spin className={this.state.ready ? "hidden" : ""}/>
            <Statistic className={this.state.ready ? "" : "hidden"} title="UI IDE Version" value={version}/>
            <Button icon={<DownloadOutlined/>} shape="round" size="large" loading={this.state.loading}
                    style={{marginTop: 16}} type="primary" onClick={this.onDownload.bind(this)}>
                Download
            </Button>
        </div>

    }
}

export default WebuiVersion;