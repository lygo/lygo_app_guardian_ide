import React, {Component} from "react";
import {Col, Row, Spin} from "antd";
import RuntimeLog from "../../components/widget-runtime-log/RuntimeLog";
import "../page.css";

class PageLogs extends Component {
    state = {
        ready: true,
    }

    constructor(props) {
        super(props);
        console.debug("construct PageLogs");
    }

    componentDidMount() {

    }

    render() {
        return <div className={this.state.ready ? "" : "content-center"}>
            <Spin className={this.state.ready ? "hidden" : ""}/>
            <div className={this.state.ready ? "" : "hidden"}>
                <Row gutter={16} style={{marginTop: 20}}>
                    <Col span={24}><RuntimeLog type="ide" heightVH={30} filename={"logging.log"}/></Col>
                </Row>
                <Row gutter={16} style={{marginTop: 20}}>
                    <Col span={24}><RuntimeLog type="runtime" heightVH={30} filename={"logging.log"}/></Col>
                </Row>
            </div>

        </div>
    }

}

export default PageLogs;