import React, {Component} from "react";
import {Button, Input, Modal, notification, PageHeader, Spin} from "antd";
import {StepForwardOutlined} from "@ant-design/icons";
import WidgetProject from "../../components/widget-project/WidgetProject";
import constants from "../../commons/constants";
import "../page.css";


class PageProjects extends Component {
    state = {
        ready: false,
        data: [],
        deploying: false,
        deployResponse: undefined,
        creating: false,    // create status
        showProjectModal: false,
    }

    constructor(props) {
        super(props);
        // console.debug(props);
        this.fields = {
            projectName: null,
            taskName: null,
            webhandlerName: null
        };
    }

    componentDidMount() {
        //console.log("PageNodes mounted.");
        this.doQuery();
    }

    componentWillUnmount() {

    }

    doQuery() {
        const url = "/dev-api/v1/files/get_project_names";
        constants.API.get(url)
            .catch(reason => console.error(url, reason))
            .then(value => {
                if (!!value) {
                    const data = value.data;
                    if (!!data) {
                        if (!!data.error) {
                            console.error(url, data.error);
                        } else if (!!data.response) {
                            // console.log("PROJECTS", data.response);
                            this.setState({
                                ready: true,
                                data: data.response,
                            })
                        }
                    }
                }
            });
    }

    setRefProjectName(element) {
        // console.log("setRefName", element);
        this.fields.projectName = element;
        if (!!this.fields.projectName) {
            this.fields.projectName.value = ""; // empty name
        }
    }

    setRefTaskName(element) {
        this.fields.taskName = element;
        if (!!this.fields.taskName) {
            this.fields.taskName.value = ""; // empty name
        }
    }

    setRefWebhandlerName(element) {
        this.fields.webhandlerName = element;
        if (!!this.fields.webhandlerName) {
            this.fields.webhandlerName.value = ""; // empty name
        }
    }

    onAddProject() {
        this.setState({showProjectModal: true})
    }

    onAddProjectConfirmed() {
        // console.log("onAddProjectConfirmed", this.fields.name.value);
        const name = !!this.fields.projectName ? this.fields.projectName.value : "";
        const taskName = !!this.fields.taskName ? this.fields.taskName.value : "";
        const webhandlerName = !!this.fields.webhandlerName ? this.fields.webhandlerName.value : "";
        if (!name) {
            return;
        }

        this.fields.projectName.value = ""; // reset
        this.fields.taskName.value = ""; // reset
        this.fields.webhandlerName.value = ""; // reset
        this.setState({creating: true, showProjectModal: false});
        const url = "/dev-api/v1/projects/create";
        constants.API.post(url, {"name": name, "task-name": taskName, "webhandler-name": webhandlerName})
            .catch(reason => console.error(url, reason))
            .then(value => {
                if (!!value) {
                    const data = value.data;
                    if (!!data) {
                        if (!!data.error) {
                            console.error(url, data.error);
                            this.notifyCreateTerminated(false, data.error);
                        } else if (!!data.response) {
                            this.notifyCreateTerminated(data.response);
                            this.doQuery();
                        }
                    }
                }
                this.setState({
                    ready: true,
                    creating: false
                })
            });
    }

    onAddProjectCancel() {
        this.setState({showProjectModal: false})
    }

    onDeploy() {
        this.setState({deploying: true});
        const url = "/dev-api/v1/compiler/deploy";
        constants.API.get(url)
            .catch(reason => console.error(url, reason))
            .then(value => {
                if (!!value) {
                    const data = value.data;
                    if (!!data) {
                        if (!!data.error) {
                            console.error(url, data.error);
                            this.notifyDeployTerminated(false, data.error);
                        } else if (!!data.response) {
                            this.notifyDeployTerminated(data.response);
                        }
                    }
                }
            });
    }

    notifyDeployTerminated(data, err) {
        if (!!data) {
            const files = data.files || [];
            const names = data.names || [];
            const message = "Deployed " + files.length + " files in " + names.length + " projects.";
            this.openNotificationWithIcon("success", "Deploy Finished", message);
        } else {
            // error
            this.openNotificationWithIcon("error", "Deploy Error", err);
        }
        this.setState({
            ready: true,
            deployResponse: data,
            deploying: false
        })
    }

    notifyCreateTerminated(data, err) {
        if (!!data) {
            const uid = data.name;
            const message = "Created Project '" + uid + "'.";
            this.openNotificationWithIcon("success", "Creation Finished", message);
        } else {
            // error
            this.openNotificationWithIcon("error", "Creation Error", err);
        }
    }

    openNotificationWithIcon(type, title, message) {
        notification[type]({
            message: title,
            description: message,
        });
    }

    render() {
        const names = this.state.data || [];
        return <div className={this.state.ready ? "" : "content-center"} style={{width: "100%"}}>
            <Spin className={this.state.ready ? "hidden" : ""}/>
            <div className={this.state.ready ? "" : "hidden"} style={{margin: "auto", width: "100%"}}>
                <PageHeader
                    className="site-page-header"
                    title="Projects"
                    subTitle={names.length}
                    extra={[
                        <Button key="1" type="primary" onClick={this.onDeploy.bind(this)}
                                loading={this.state.deploying} icon={<StepForwardOutlined/>}>
                            Deploy
                        </Button>,
                    ]}
                />
                <div style={{
                    display: "flex",
                    flexDirection: "row",
                    flexWrap: "wrap",
                    justifyContent: "start",
                    width: "100%"
                }}>
                    {
                        names.map(name => {
                            return <WidgetProject key={name} name={name}/>
                        })
                    }
                    <WidgetProject onAddProject={this.onAddProject.bind(this)} key={"+"} name={"+"}/>
                </div>

                <Modal title="New BackEnd Project" visible={this.state.showProjectModal}
                       onOk={this.onAddProjectConfirmed.bind(this)} onCancel={this.onAddProjectCancel.bind(this)}>
                    <p>Select a unique name for your project</p>
                    <Input ref={this.setRefProjectName.bind(this)} onChange={(e) => {
                        this.fields.projectName.value = e.target.value
                    }} placeholder="Project Name"/>
                    <p>Add a Task</p>
                    <Input ref={this.setRefTaskName.bind(this)} onChange={(e) => {
                        this.fields.taskName.value = e.target.value
                    }} placeholder="Task Name"/>
                    <p>Add an Endpoint</p>
                    <Input ref={this.setRefWebhandlerName.bind(this)} onChange={(e) => {
                        this.fields.webhandlerName.value = e.target.value
                    }} placeholder="Endpoint Name"/>
                </Modal>

            </div>
        </div>
    }

}

export default PageProjects;