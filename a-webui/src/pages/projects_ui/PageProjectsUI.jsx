import React, {Component} from "react";
import {Button, Input, Modal, notification, PageHeader, Spin} from "antd";
import {PlayCircleOutlined, StepForwardOutlined, StopOutlined} from "@ant-design/icons";
import WidgetProjectUI from "../../components/widget-project-ui/WidgetProjectUI";
import constants from "../../commons/constants";
import "../page.css";


class PageProjectsUI extends Component {
    state = {
        ready: false,
        data: [],
        deploying: false,   // deploy status
        launching: false,   // launch status
        running: false,   // dev session already running
        deployResponse: undefined,
        creating: false,    // create status
        showProjectModal: false,
    }

    constructor(props) {
        super(props);
        // edit fields
        this.fields = {name: null};
    }

    componentDidMount() {
        //console.log("PageNodes mounted.");
        this.doQuery();
        this.refreshRunningStatus();
    }

    componentWillUnmount() {

    }

    setRefName(element) {
        // console.log("setRefName", element);
        this.fields.name = element;
        if (!!this.fields.name) {
            this.fields.name.value = ""; // empty name
        }
    }

    doQuery() {
        const url = "/dev-api/v1/files/get_project_ui_names";
        constants.API.get(url)
            .catch(reason => console.error(url, reason))
            .then(value => {
                if (!!value) {
                    const data = value.data;
                    if (!!data) {
                        if (!!data.error) {
                            console.error(url, data.error);
                        } else if (!!data.response) {
                            // console.log("PROJECTS", data.response);
                            this.setState({
                                ready: true,
                                data: data.response,
                            })
                        }
                    }
                }
            });
    }

    onLaunchProject() {
        this.setState({launching: true});
        const url = "/dev-api/v1/compiler/start_ui";
        constants.API.get(url)
            .catch(reason => console.error(url, reason))
            .then(value => {
                if (!!value) {
                    const data = value.data;
                    if (!!data) {
                        if (!!data.error) {
                            console.error(url, data.error);
                        }
                    }
                }
                this.setState({launching: false});
                this.refreshRunningStatus();
            });
    }

    onStopProject() {
        this.setState({launching: true});
        const url = "/dev-api/v1/compiler/stop_ui";
        constants.API.get(url)
            .catch(reason => console.error(url, reason))
            .then(value => {
                if (!!value) {
                    const data = value.data;
                    if (!!data) {
                        if (!!data.error) {
                            console.error(url, data.error);
                        }
                    }
                }
                this.setState({launching: false});
                this.refreshRunningStatus();
            });
    }

    onAddProject() {
        this.setState({showProjectModal: true})
    }

    onAddProjectConfirmed() {
        // console.log("onAddProjectConfirmed", this.fields.name.value);
        const name = !!this.fields.name ? this.fields.name.value : "";
        if (!name) {
            return;
        }

        this.fields.name.value = "";
        this.setState({creating: true, showProjectModal: false});
        const url = "/dev-api/v1/files/project_ui_create";
        constants.API.post(url, {name: name, framework: "react"})
            .catch(reason => console.error(url, reason))
            .then(value => {
                if (!!value) {
                    const data = value.data;
                    if (!!data) {
                        if (!!data.error) {
                            console.error(url, data.error);
                            this.notifyCreateTerminated(false, data.error);
                        } else if (!!data.response) {
                            this.notifyCreateTerminated(data.response);
                            this.doQuery();
                        }
                    }
                }
                this.setState({
                    ready: true,
                    creating: false
                })
            });
    }

    onAddProjectCancel() {
        this.setState({showProjectModal: false})
    }

    onDeploy() {
        this.setState({deploying: true});
        const url = "/dev-api/v1/compiler/deploy_ui";
        constants.API.get(url)
            .catch(reason => console.error(url, reason))
            .then(value => {
                if (!!value) {
                    const data = value.data;
                    if (!!data) {
                        if (!!data.error) {
                            console.error(url, data.error);
                            this.notifyDeployTerminated(false, data.error);
                        } else if (!!data.response) {
                            this.notifyDeployTerminated(data.response);
                        }
                    }
                }
            });
    }

    refreshRunningStatus() {
        const url = "/dev-api/v1/compiler/started_ui";
        constants.API.get(url)
            .catch(reason => console.error(url, reason))
            .then(value => {
                if (!!value) {
                    const data = value.data;
                    if (!!data) {
                        if (!!data.error) {
                            console.error(url, data.error);
                            this.setState({running: false});
                        } else if (!!data.response) {
                            this.setState({running: data.response === "true"});
                        }
                    }
                }
            });
    }

    notifyDeployTerminated(data, err) {
        if (!!data) {
            const files = data.files || [];
            const names = data.names || [];
            const message = "Deployed " + files.length + " files in " + names.length + " projects.";
            this.openNotificationWithIcon("success", "Deploy Finished", message);
        } else {
            // error
            this.openNotificationWithIcon("error", "Deploy Error", err);
        }
        this.setState({
            ready: true,
            deployResponse: data,
            deploying: false
        })
    }

    notifyCreateTerminated(data, err) {
        if (!!data) {
            const uid = data.uid;
            const message = "Created Project '" + uid + "'.";
            this.openNotificationWithIcon("success", "Creation Finished", message);
        } else {
            // error
            this.openNotificationWithIcon("error", "Creation Error", err);
        }
    }

    openNotificationWithIcon(type, title, message) {
        notification[type]({
            message: title,
            description: message,
        });
    }

    render() {
        const names = this.state.data || [];
        const running = this.state.running;
        return <div className={this.state.ready ? "" : "content-center"} style={{width: "100%"}}>
            <Spin className={this.state.ready ? "hidden" : ""}/>
            <div className={this.state.ready ? "" : "hidden"} style={{margin: "auto", width: "100%"}}>
                <PageHeader
                    className="site-page-header"
                    title="Projects"
                    subTitle={names.length}
                    extra={[
                        <Button key="2" type="primary" onClick={this.onLaunchProject.bind(this)}
                                className={running ? "hidden" : ""}
                                loading={this.state.launching} icon={<PlayCircleOutlined/>}>
                            Start
                        </Button>,
                        <Button key="3" type="primary" danger onClick={this.onStopProject.bind(this)}
                                className={running ? "" : "hidden"}
                                loading={this.state.launching} icon={<StopOutlined/>}>
                            Stop
                        </Button>,
                        <Button key="1" type="default" onClick={this.onDeploy.bind(this)}
                                loading={this.state.deploying} icon={<StepForwardOutlined/>}>
                            Deploy
                        </Button>,
                    ]}
                />
                <div style={{
                    display: "flex",
                    flexDirection: "row",
                    flexWrap: "wrap",
                    justifyContent: "start",
                    width: "100%"
                }}>
                    {
                        names.map(name => {
                            return <WidgetProjectUI key={name} name={name}/>
                        })
                    }
                    <WidgetProjectUI loading={this.state.creating} onAddProject={this.onAddProject.bind(this)} key={"+"}
                                     name={"+"}/>
                </div>
                <Modal title="New UI Project" visible={this.state.showProjectModal}
                       onOk={this.onAddProjectConfirmed.bind(this)} onCancel={this.onAddProjectCancel.bind(this)}>
                    <p>Select a unique name for your project</p>
                    <Input ref={this.setRefName.bind(this)} onChange={(e) => {
                        this.fields.name.value = e.target.value
                    }} placeholder="Project Name"/>
                </Modal>
            </div>
        </div>
    }

}

export default PageProjectsUI;