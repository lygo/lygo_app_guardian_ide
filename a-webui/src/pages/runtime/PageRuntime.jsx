import React, {Component} from "react";
import {Col, Row, Spin} from "antd";
//-- STYLE --//
import "../page.css";
import WebuiVersion from "../../components/widget-webui-version/WebuiVersion";
import RuntimeStatus from "../../components/widget-runtime-status/RuntimeStatus";
import RuntimeRemote from "../../components/widget-runtime-remote/RuntimeRemote";
import RuntimeLog from "../../components/widget-runtime-log/RuntimeLog";


class PageRuntime extends Component {
    state = {
        ready: false,
        data: [],
    }

    constructor(props) {
        super(props);
        console.debug(props);
    }

    componentDidMount() {
        //console.log("PageNodes mounted.");
        this.setState({ready: true})
    }


    render() {
        return <div className={this.state.ready ? "" : "content-center"}>
            <Spin className={this.state.ready ? "hidden" : ""}/>
            <div className={this.state.ready ? "" : "hidden"}>
                <Row gutter={16}>
                    <Col span={8}><WebuiVersion/></Col>
                    <Col span={8}><RuntimeStatus/></Col>
                    <Col span={8}><RuntimeRemote/></Col>
                </Row>
                <Row gutter={16} style={{marginTop: 20}}>
                    <Col span={24}><RuntimeLog filename={"logging.log"}/></Col>
                </Row>
            </div>

        </div>
    }

}

export default PageRuntime;