package app

import (
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/commons"
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/development"
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/rt"
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/secure"
	"bitbucket.org/lygo/lygo_commons/lygo_exec"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_events"
	"path/filepath"
)

type App struct {
	root     string
	dirStart string
	dirApp   string
	dirWork  string
	settings *commons.GuardianSettings
	logger   *commons.Logger
	mode     string
	stopChan chan bool
	events   *lygo_events.Emitter

	secureManager *secure.AppSecure
	devserver     *development.DevWebserver
	guardian      *rt.GuardianRuntime
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewApp(mode string, l *commons.Logger) *App {
	instance := new(App)
	instance.mode = mode
	instance.logger = l //commons.NewLogger(mode)

	instance.settings, _ = commons.NewGuardianSettings(mode)
	instance.dirStart = lygo_paths.GetWorkspace(commons.WpDirStart).GetPath()
	instance.dirApp = lygo_paths.GetWorkspace(commons.WpDirApp).GetPath()
	instance.dirWork = lygo_paths.GetWorkspace(commons.WpDirWork).GetPath()
	instance.root = filepath.Dir(instance.dirWork)


	instance.stopChan = make(chan bool, 1)
	instance.events = lygo_events.NewEmitter()

	instance.guardian = rt.NewGuardianRuntime(instance.dirApp, instance.dirWork, commons.DirDev,
		instance.logger, instance.events)
	instance.secureManager = secure.NewAppSecure(mode, instance.dirWork,
		instance.logger)
	instance.devserver = development.NewDevWebserver(mode,
		instance.logger, instance.events, instance.secureManager, instance.guardian)

	// handle internal events
	instance.events.On(commons.EventOnDoStop, instance.handleDoStopEvent)

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *App) GetLogger() *commons.Logger {
	if nil != instance {
		return instance.logger
	}
	return nil
}

func (instance *App) Start() (err error) {
	if nil != instance {
		// start scheduler
		err = instance.start()

		if nil == err {
			// launch browser
			local := instance.devserver.LocalUrl()
			if len(local) > 0 {
				err = lygo_exec.Open(local)
			}
		}

		return err
	}
	return commons.PanicSystemError
}

func (instance *App) Stop() (err error) {
	if nil != instance {
		// start scheduler
		err = instance.stop()
		return
	}
	return commons.PanicSystemError
}

func (instance *App) Wait() {
	// wait exit
	<-instance.stopChan
	// reset channel
	instance.stopChan = make(chan bool, 1)
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *App) start() (err error) {
	if nil != instance {

		_ = instance.secureManager.Start()
		instance.devserver.Start()

		err = instance.guardian.Start()

		return
	}
	return commons.PanicSystemError
}

func (instance *App) stop() error {
	if nil != instance {
		if nil != instance.stopChan {

			_ = instance.guardian.Stop()
			instance.devserver.Stop()
			instance.secureManager.Stop()

			instance.stopChan <- true
			instance.stopChan = nil
		}
		return nil
	}
	return commons.PanicSystemError
}

func (instance *App) handleDoStopEvent(_ *lygo_events.Event) {
	if nil != instance {
		_ = instance.Stop()
	}
}
