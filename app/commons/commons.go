package commons

import (
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"errors"
	"strings"
)

const (
	Version = "1.1.23"
	Name    = "THE GUARDIAN IDE"

	ModeProduction = "production"
	ModeDebug      = "debug"

	EventOnUpgrade      = "on_upgrade"
	EventOnRuntimeStart = "on_rt_start" // guardian runtime started
	EventOnRuntimeStop  = "on_rt_quit"  // guardian runtime stopped
	EventOnError        = "on_error"
	EventOnDoStop       = "on_do_stop"

	// workspaces
	WpDirStart = "start"
	WpDirApp   = "app"
	WpDirWork  = "*"

	DirDevelopment = "_development"
)

const CAT_LIBS = "libs"
const CAT_PROJECTS = "projects"

const SUB_COMMONS = "commons"
const SUB_TASKS = "tasks"
const SUB_WEBHANDLERS = "webhandlers"

const TYPE_PROJECT = "project"
const TYPE_RESOURCE = "resource"
const TYPE_DEPLOY = "deploy"
const TYPE_UNDEFINED = "undefined"

// ---------------------------------------------------------------------------------------------------------------------
//		e r r o r s
// ---------------------------------------------------------------------------------------------------------------------

var (
	PanicSystemError          = errors.New("panic_system_error")
	InvalidConfigurationError = errors.New("invalid_configuration_error")
	MissingFileNameError      = errors.New("missing_file_name")

	EndpointNotReadyError = errors.New("endpoint_not_ready")

	ProjectAlreadyExistsError = errors.New("project_already_exists")

	HttpUnauthorizedError                 = errors.New("unauthorized")                   // 401
	AccessTokenExpiredError               = errors.New("access_token_expired")           // 401
	RefreshTokenExpiredError              = errors.New("refresh_token_expired")          // 401
	HttpUnsupportedAuthorizationTypeError = errors.New("unsupported_authorization_type") // 401
)

// ---------------------------------------------------------------------------------------------------------------------
//		p a t h s
// ---------------------------------------------------------------------------------------------------------------------

var DirWork string
var DirWorkPrograms string
var DirDev string
var DirDevBuild string
var DirDevSources string
var DirDevProjects string
var DirDevLibs string
var DirDevUI string

func InitPaths() {
	DirWork = lygo_paths.Absolute(lygo_paths.GetWorkspace(WpDirWork).GetPath())
	DirWorkPrograms = lygo_paths.Concat(DirWork, "programs")
	// dev
	DirDev = lygo_paths.Concat(DirWork, DirDevelopment)
	DirDevBuild = lygo_paths.Concat(DirDev, "build")
	DirDevSources = lygo_paths.Concat(DirDev, "sources")
	DirDevProjects = lygo_paths.Concat(DirDevSources, CAT_PROJECTS)
	DirDevLibs = lygo_paths.Concat(DirDevSources, CAT_LIBS)
	// dev ui
	DirDevUI = lygo_paths.Concat(DirWork, "_development_ui")
}

func GetProjectRoot(name string) string {
	return lygo_paths.Concat(DirDevProjects, name)
}

func GetProjectRelativePath(projectName, fullPath string) string {
	root := GetProjectRoot(projectName)
	if strings.Index(fullPath, root) == 0 {
		return strings.Replace(fullPath, root, ".", 1)
	}
	return fullPath
}

func GetProjectMainFileName(name string) string {
	return lygo_paths.Concat(DirDevProjects, name, "main.json")
}

func GetProjectUIMainFileName(name string) string {
	return lygo_paths.Concat(DirDevUI, name, "main.json")
}
