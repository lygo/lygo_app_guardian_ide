package commons

import (
	"bytes"
	"strings"
	"text/template"
)

const TplFileSettings = `
{
  "silent": true,
  "log-level": "{{.Mode}}",
  "actions": []
}
`

const TplFileWebserver = `
{
  "enabled": false,
  "http": {
    "root": "./webserver",
    "server": {
      "enable_request_id": true,
      "prefork": false
    },
    "hosts": [
      {
        "addr": ":80",
        "tls": false,
        "websocket": {
          "enabled": true
        }
      },
      {
        "addr": ":443",
        "tls": true,
        "ssl_cert": "./cert/ssl-cert.pem",
        "ssl_key": "./cert/ssl-cert.key",
        "websocket": {
          "enabled": true
        }
      }
    ],
    "static": [
      {
        "enabled": true,
        "prefix": "/",
        "root": "./www",
        "index": "",
        "compress": true
      }
    ],
    "compression": {
      "enabled": false,
      "level": 0
    },
    "limiter": {
      "enabled": false,
      "timeout": 30,
      "max": 10
    },
    "CORS": {
      "enabled": true
    }
  },
  "initialize": [
    {
      "uid": "webserver_init",
      "program": "./programs/webserver/tasks/init/init.js",
      "variables": {
        "PORT": 10001
      }
    }
  ],
  "routing": [
    {
      "method": "ALL",
      "endpoint": "/api/v4/test/*",
      "action": {
        "uid": "endpoint_test",
        "program": "./programs/webhandlers/test_api.js",
        "variables": {
          "PORT": 10001,
          "authorization": {
            "type": "none",
            "value": ""
          },
          "database": {
            "driver": "arango",
            "dsn": "root:password@tcp(localhost:8529)/test)"
          }
        }
      }
    }
  ]
}
`

const TplFileUpdater = `
{
  "enabled": false,
  "updaters": [
    {
      "uid": "nio_server",
      "version_file": "http://localhost/scripts/nio_server/version.txt",
      "package_files": [
        {
          "file": "http://localhost/scripts/nio_server/nio_server.js",
          "target": "$dir_program"
        }
      ],
      "scheduled_updates": [
        {
          "uid": "every_5_seconds",
          "start_at": "",
          "timeline": "second:5"
        }
      ]
    }
  ]
}
`

const TplFileSecure = `
{
  "secrets": {
    "auth": "this-is-token-to-authenticate",
    "access": "hsdfuhksdhf5435khjsd",
    "refresh": "hsdfuhqswe34qwksdhfkhjsd"
  },
  "cache-storage": {
    "driver": "bolt",
    "dsn": "root:root@file:./_guardian/data/cache.dat"
  },
  "auth-storage": {
    "driver": "bolt",
    "dsn": "root:root@file:./_guardian/data/cache.dat"
  }
}
`

const TplFileDevelopment = `
{
  "enabled": true,
  "http": {
    "root": "./_development/_webui",
    "server": {
      "enable_request_id": true,
      "prefork": false
    },
    "hosts": [
      {
        "addr": ":9090",
        "tls": false,
        "websocket": {
          "enabled": true
        }
      },
      {
        "addr": ":9091",
        "tls": true,
        "ssl_cert": "./cert/ssl-cert.pem",
        "ssl_key": "./cert/ssl-cert.key",
        "websocket": {
          "enabled": true
        }
      }
    ],
    "static": [
      {
        "enabled": true,
        "prefix": "/",
        "root": "./www",
        "index": "",
        "compress": true
      }
    ],
    "compression": {
      "enabled": false,
      "level": 0
    },
    "limiter": {
      "enabled": false,
      "timeout": 30,
      "max": 10
    },
    "CORS": {
      "enabled": true
    }
  },
  "authorization": {
    "type": "base",
    "value": "MTIzOmFiYw=="
  }
}
`

const TplFileUI = `
{
  "enabled": false,
  "home": ""
}
`


func MergeTpl(text string, data interface{}) (string, error) {
	if nil==data{
		data = struct {}{}
	}
	buff := bytes.NewBufferString("")
	t := template.Must(template.New("template").Parse(strings.Trim(text, "\n")))
	err := t.Execute(buff, data)
	if nil != err {
		return "", err
	}
	return buff.String(), err
}