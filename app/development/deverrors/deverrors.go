package deverrors

import "errors"

var (
	FileExistsDevError   = errors.New("file_already_exists")
	FileNotFoundDevError = errors.New("file_not_found")
	PathNotAllowedDevError = errors.New("path_not_allowed")
	NodeNotInstalledDevError = errors.New("node_not_installed")
)
