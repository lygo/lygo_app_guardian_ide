package development

import (
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/commons"
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/development/deverrors"
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/development/projects"
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/development/projects_ui"
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/http"
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/rt"
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/secure"
	"bitbucket.org/lygo/lygo_commons/lygo_exec"
	"bitbucket.org/lygo/lygo_commons/lygo_exec/console_program/vscode"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"bitbucket.org/lygo/lygo_events"
	"github.com/gofiber/fiber/v2"
	"os"
	"strings"
	"sync"
)

// ---------------------------------------------------------------------------------------------------------------------
//		t y p e
// ---------------------------------------------------------------------------------------------------------------------

type DevWebserver struct {
	mode          string
	dirWork       string // guardian workspace
	dirDev        string
	logger        *commons.Logger
	events        *lygo_events.Emitter
	secureManager *secure.AppSecure
	guardian      *rt.GuardianRuntime

	mux              sync.Mutex
	isRuntimeWorking bool
	webserver        *http.Webserver
	controller       *projects.DevController
	controllerUI     *projects_ui.DevUIController
}

func NewDevWebserver(mode string, l *commons.Logger, e *lygo_events.Emitter, s *secure.AppSecure, g *rt.GuardianRuntime) *DevWebserver {
	instance := new(DevWebserver)
	instance.mode = mode
	instance.dirWork = commons.DirWork
	instance.dirDev = commons.DirDev
	instance.logger = l
	instance.events = e
	instance.secureManager = s
	instance.guardian = g

	instance.isRuntimeWorking = false
	instance.webserver = http.NewWebserver("development", mode, commons.DirWork)
	instance.controller = projects.NewDevController(l)
	instance.controllerUI = projects_ui.NewDevUIController(g.GetStaticWWWRoot(), l)

	_ = instance.init()

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevWebserver) Start() {
	if nil != instance && nil != instance.webserver {
		if instance.webserver.Start() {
			// need some more initialization
		}
	}
}

func (instance *DevWebserver) Stop() {
	if nil != instance && nil != instance.webserver {
		instance.webserver.Stop()
	}
}

func (instance *DevWebserver) DirDevFiles() string {
	return instance.dirDev
}

func (instance *DevWebserver) LocalUrl() string {
	if nil != instance && nil != instance.webserver {
		return instance.webserver.LocalUrl()
	}
	return ""
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevWebserver) init() error {
	err := instance.webserver.Initialize(nil)
	if nil == err && instance.webserver.IsEnabled() {
		// init folders
		_ = lygo_paths.Mkdir(instance.webserver.HttpRoot() + string(os.PathSeparator))
		_ = lygo_paths.Mkdir(instance.webserver.HttpStaticRoot() + string(os.PathSeparator))
		return instance.initHandlers()
	}

	instance.events.On(commons.EventOnRuntimeStart, instance.handleRuntimeStarted)
	instance.events.On(commons.EventOnRuntimeStop, instance.handleRuntimeStopped)

	return err
}

func (instance *DevWebserver) handleRuntimeStarted(_ *lygo_events.Event) {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	instance.isRuntimeWorking = true
}

func (instance *DevWebserver) handleRuntimeStopped(_ *lygo_events.Event) {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	instance.isRuntimeWorking = false
}

func (instance *DevWebserver) initHandlers() error {
	err := instance.webserver.Initialize(nil)

	//-- utils --//
	instance.webserver.Handle("get", "/dev-api/v1/utils/version", instance.handleVersion)
	instance.webserver.Handle("get", "/dev-api/v1/utils/error", instance.handleInitError)

	//-- webui --//
	instance.webserver.Handle("get", "/dev-api/v1/webui/version", instance.handleWebUiVersion)
	instance.webserver.Handle("get", "/dev-api/v1/webui/remote_version", instance.handleWebUiRemoteVersion)
	instance.webserver.Handle("get", "/dev-api/v1/webui/remote_download", instance.handleWebUiRemoteDownload)

	// ide
	instance.webserver.Handle("get", "/dev-api/v1/ide/get_log", instance.handleIDEGetLog)
	instance.webserver.Handle("get", "/dev-api/v1/ide/open_log_dir", instance.handleIDEOpenLogDir)
	instance.webserver.Handle("get", "/dev-api/v1/ide/open_log_file", instance.handleIDEOpenLogFile)
	instance.webserver.Handle("get", "/dev-api/v1/ide/clear_log_file", instance.handleIDEClearLogFile)

	//-- runtime --//
	instance.webserver.Handle("get", "/dev-api/v1/runtime/is_running", instance.handleRuntimeIsRunning)
	instance.webserver.Handle("get", "/dev-api/v1/runtime/version", instance.handleRuntimeVersion)
	instance.webserver.Handle("get", "/dev-api/v1/runtime/start", instance.handleRuntimeStart)
	instance.webserver.Handle("get", "/dev-api/v1/runtime/stop", instance.handleRuntimeStop)
	instance.webserver.Handle("get", "/dev-api/v1/runtime/restart", instance.handleRuntimeRestart)
	instance.webserver.Handle("get", "/dev-api/v1/runtime/remote_version", instance.handleRuntimeLatestVersion)
	instance.webserver.Handle("get", "/dev-api/v1/runtime/remote_download", instance.handleRuntimeRemoteDownload)
	instance.webserver.Handle("get", "/dev-api/v1/runtime/get_log", instance.handleRuntimeGetLog)
	instance.webserver.Handle("get", "/dev-api/v1/runtime/open_log_dir", instance.handleRuntimeOpenLogDir)
	instance.webserver.Handle("get", "/dev-api/v1/runtime/open_log_file", instance.handleRuntimeOpenLogFile)
	instance.webserver.Handle("get", "/dev-api/v1/runtime/clear_log_file", instance.handleRuntimeClearLogFile)

	//-- projects --//
	instance.webserver.Handle("post", "/dev-api/v1/projects/get", instance.handleGetProject)
	instance.webserver.Handle("post", "/dev-api/v1/projects/create", instance.handleProjectCreate)

	//-- files --//
	instance.webserver.Handle("get", "/dev-api/v1/files/reload", instance.handleReload)
	instance.webserver.Handle("get", "/dev-api/v1/files/get_libs", instance.handleGetLibs)
	instance.webserver.Handle("get", "/dev-api/v1/files/get_projects", instance.handleGetProjects)
	instance.webserver.Handle("get", "/dev-api/v1/files/get_project_info", instance.handleGetProjectInfo)
	instance.webserver.Handle("get", "/dev-api/v1/files/get_project_names", instance.handleGetProjectsNames)
	instance.webserver.Handle("get", "/dev-api/v1/files/get_project_files", instance.handleGetProjectFiles)
	instance.webserver.Handle("get", "/dev-api/v1/files/get_project_settings", instance.handleGetProjectSettings)
	instance.webserver.Handle("get", "/dev-api/v1/files/activate_project", instance.handleActivateProject)
	instance.webserver.Handle("get", "/dev-api/v1/files/get_lib_files", instance.handleGetLibFiles)
	instance.webserver.Handle("get", "/dev-api/v1/files/get_file_info", instance.handleGetFileInfo)
	instance.webserver.Handle("post", "/dev-api/v1/files/update_file", instance.handleUpdateFile)
	instance.webserver.Handle("get", "/dev-api/v1/files/open_project_dir", instance.handleProjectOpenDir)
	instance.webserver.Handle("get", "/dev-api/v1/files/open_project_vscode", instance.handleProjectOpenVSCode)

	//-- compiler --//
	instance.webserver.Handle("get", "/dev-api/v1/compiler/deploy", instance.handleCompilerDeploy)
	instance.webserver.Handle("get", "/dev-api/v1/compiler/deploy_ui", instance.handleCompilerDeployUI)
	instance.webserver.Handle("get", "/dev-api/v1/compiler/start_ui", instance.handleCompilerStartUI)
	instance.webserver.Handle("get", "/dev-api/v1/compiler/started_ui", instance.handleCompilerStartedUI)
	instance.webserver.Handle("get", "/dev-api/v1/compiler/stop_ui", instance.handleCompilerStopUI)

	//-- files UI --//
	instance.webserver.Handle("get", "/dev-api/v1/files/reload_ui", instance.handleReloadUI)
	instance.webserver.Handle("get", "/dev-api/v1/files/get_projects_ui", instance.handleGetProjectsUI)
	instance.webserver.Handle("get", "/dev-api/v1/files/get_project_ui_names", instance.handleGetProjectsUINames)
	instance.webserver.Handle("get", "/dev-api/v1/files/get_project_ui_info", instance.handleGetProjectUIInfo)
	instance.webserver.Handle("get", "/dev-api/v1/files/activate_project_ui", instance.handleActivateProjectUI)
	instance.webserver.Handle("get", "/dev-api/v1/files/open_project_ui_dir", instance.handleProjectUIOpenDir)
	instance.webserver.Handle("get", "/dev-api/v1/files/open_project_ui_vscode", instance.handleProjectUIOpenVSCode)
	instance.webserver.Handle("post", "/dev-api/v1/files/project_ui_start", instance.handleProjectUIStart)
	instance.webserver.Handle("post", "/dev-api/v1/files/project_ui_create", instance.handleProjectUICreate)

	return err
}

// ---------------------------------------------------------------------------------------------------------------------
// 	u t i l s
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevWebserver) handleVersion(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		return http.WriteResponse(ctx, commons.Version, nil)
	}
	return nil
}

func (instance *DevWebserver) handleInitError(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		err := instance.controller.GetError()
		if nil != err {
			return http.WriteResponse(ctx, err.Error(), nil)
		}
		return http.WriteResponse(ctx, "", nil)
	}
	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
// 	web ui
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevWebserver) handleWebUiVersion(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		return http.WriteResponse(ctx, instance.guardian.CurrentUiVersion(), nil)
	}
	return nil
}

func (instance *DevWebserver) handleWebUiRemoteVersion(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		return http.WriteResponse(ctx, instance.guardian.LatestVersion(), nil)
	}
	return nil
}

func (instance *DevWebserver) handleWebUiRemoteDownload(ctx *fiber.Ctx) (err error) {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		err = instance.guardian.LatestUiDownload()
		if nil == err {
			// should reload UI
			// err = instance.guardian.Start()
		} else {
			instance.logger.Error("DevWebserver.handleWebUiRemoteDownload()", err)
		}
		return http.WriteResponse(ctx, instance.guardian.CurrentUiVersion(), err)
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
// 	ide
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevWebserver) handleIDEGetLog(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		log, err := instance.guardian.GetLog()
		return http.WriteResponse(ctx, log, err)
	}
	return nil
}

func (instance *DevWebserver) handleIDEOpenLogDir(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		err := instance.guardian.OpenLogDir()
		return http.WriteResponse(ctx, nil == err, err)
	}
	return nil
}

func (instance *DevWebserver) handleIDEOpenLogFile(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		err := instance.guardian.OpenLogFile()
		return http.WriteResponse(ctx, nil == err, err)
	}
	return nil
}

func (instance *DevWebserver) handleIDEClearLogFile(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		err := instance.guardian.ClearLogFile()
		return http.WriteResponse(ctx, nil == err, err)
	}
	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
// 	r u n t i m e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevWebserver) handleRuntimeIsRunning(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		return http.WriteResponse(ctx, instance.guardian.IsRunning(), nil)
	}
	return nil
}

func (instance *DevWebserver) handleRuntimeVersion(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		return http.WriteResponse(ctx, strings.Trim(instance.guardian.CurrentVersion(), " \n"), nil)
	}
	return nil
}

func (instance *DevWebserver) handleRuntimeStart(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		err := instance.guardian.Start()
		return http.WriteResponse(ctx, instance.guardian.IsRunning(), err)
	}
	return nil
}

func (instance *DevWebserver) handleRuntimeStop(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		err := instance.guardian.Stop()
		return http.WriteResponse(ctx, instance.guardian.IsRunning(), err)
	}
	return nil
}

func (instance *DevWebserver) handleRuntimeRestart(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		err := instance.guardian.Restart()
		return http.WriteResponse(ctx, instance.guardian.IsRunning(), err)
	}
	return nil
}

func (instance *DevWebserver) handleRuntimeLatestVersion(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		return http.WriteResponse(ctx, instance.guardian.LatestVersion(), nil)
	}
	return nil
}

func (instance *DevWebserver) handleRuntimeRemoteDownload(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		err := instance.guardian.Stop()
		if nil == err {
			err = instance.guardian.LatestDownload()
			if nil == err {
				err = instance.guardian.Start()
			}
		}
		return http.WriteResponse(ctx, instance.guardian.CurrentVersion(), err)
	}
	return nil
}

func (instance *DevWebserver) handleRuntimeGetLog(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		log, err := instance.guardian.GetRuntimeLog()
		return http.WriteResponse(ctx, log, err)
	}
	return nil
}

func (instance *DevWebserver) handleRuntimeOpenLogDir(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		err := instance.guardian.OpenRuntimeLogDir()
		return http.WriteResponse(ctx, nil == err, err)
	}
	return nil
}

func (instance *DevWebserver) handleRuntimeOpenLogFile(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		err := instance.guardian.OpenRuntimeLogFile()
		return http.WriteResponse(ctx, nil == err, err)
	}
	return nil
}

func (instance *DevWebserver) handleRuntimeClearLogFile(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		err := instance.guardian.ClearRuntimeLogFile()
		return http.WriteResponse(ctx, nil == err, err)
	}
	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
// 	p r o j e c t s
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevWebserver) handleGetProject(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		params, err := http.AssertParams(ctx, []string{"name"})
		if nil != err {
			return http.WriteResponse(ctx, nil, err)
		}
		name := lygo_reflect.GetString(params, "name")
		response := instance.controller.Files().GetProject(name)
		return http.WriteResponse(ctx, response, nil)
	}
	return nil
}

func (instance *DevWebserver) handleProjectCreate(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		_, err := http.AssertParams(ctx, []string{"name"})
		if nil != err {
			return http.WriteResponse(ctx, nil, err)
		}
		params := http.Params(ctx, false)
		name := lygo_reflect.GetString(params, "name")
		taskName := lygo_reflect.GetString(params, "task-name")
		webhandlerName := lygo_reflect.GetString(params, "webhandler-name")

		// projects cannot be empty
		if len(taskName) == 0 && len(webhandlerName) == 0 {
			taskName = "sampleTask"
		}

		// creates project
		response, err := instance.controller.NewProject(name, taskName, webhandlerName)
		if nil != err {
			return http.WriteResponse(ctx, nil, err)
		}
		return http.WriteResponse(ctx, response, nil)
	}
	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
// 	f i l e s
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevWebserver) handleReload(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		return http.WriteResponse(ctx, instance.controller.Files().Reload(), nil)
	}
	return nil
}

func (instance *DevWebserver) handleGetLibs(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		return http.WriteResponse(ctx, instance.controller.Files().GetLibs(), nil)
	}
	return nil
}

func (instance *DevWebserver) handleGetProjects(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		return http.WriteResponse(ctx, instance.controller.Files().GetProjects(), nil)
	}
	return nil
}

func (instance *DevWebserver) handleGetProjectsNames(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		return http.WriteResponse(ctx, instance.controller.Files().GetProjectsNames(), nil)
	}
	return nil
}

func (instance *DevWebserver) handleGetProjectInfo(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		params, err := http.AssertParams(ctx, []string{"name"})
		if nil != err {
			return http.WriteResponse(ctx, nil, err)
		}
		name := lygo_reflect.GetString(params, "name")
		response := instance.controller.Files().GetProjectInfo(name)
		return http.WriteResponse(ctx, response, nil)
	}
	return nil
}

func (instance *DevWebserver) handleGetProjectFiles(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		params, err := http.AssertParams(ctx, []string{"name"})
		if nil != err {
			return http.WriteResponse(ctx, nil, err)
		}
		name := lygo_reflect.GetString(params, "name")
		response := instance.controller.Files().GetProjectFiles(name)
		return http.WriteResponse(ctx, response, nil)
	}
	return nil
}

func (instance *DevWebserver) handleGetProjectSettings(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		params, err := http.AssertParams(ctx, []string{"name"})
		if nil != err {
			return http.WriteResponse(ctx, nil, err)
		}
		name := lygo_reflect.GetString(params, "name")
		response := instance.controller.Files().GetProjectFilesDeploy(name) // settings to deploy
		return http.WriteResponse(ctx, response, nil)
	}
	return nil
}

func (instance *DevWebserver) handleActivateProject(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		params, err := http.AssertParams(ctx, []string{"name"})
		if nil != err {
			return http.WriteResponse(ctx, nil, err)
		}
		name := lygo_reflect.GetString(params, "name")
		response := instance.controller.Files().ActivateProject(name)
		return http.WriteResponse(ctx, response, nil)
	}
	return nil
}

func (instance *DevWebserver) handleGetLibFiles(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		params, err := http.AssertParams(ctx, []string{"name"})
		if nil != err {
			return http.WriteResponse(ctx, nil, err)
		}
		name := lygo_reflect.GetString(params, "name")
		response := instance.controller.Files().GetLibFiles(name)
		return http.WriteResponse(ctx, response, nil)
	}
	return nil
}

func (instance *DevWebserver) handleGetFileInfo(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		params, err := http.AssertParams(ctx, []string{"id"})
		if nil != err {
			return http.WriteResponse(ctx, nil, err)
		}
		value := lygo_reflect.GetString(params, "id")
		response := instance.controller.Files().Get(value)
		if nil == response {
			return http.WriteResponse(ctx, nil, deverrors.FileNotFoundDevError)
		}
		return http.WriteResponse(ctx, response.Props(), nil)
	}
	return nil
}

func (instance *DevWebserver) handleUpdateFile(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		params, err := http.AssertParams(ctx, []string{"id", "contend"})
		if nil != err {
			return http.WriteResponse(ctx, nil, err)
		}
		value := lygo_reflect.GetString(params, "id")
		response := instance.controller.Files().Get(value)
		if nil == response {
			return http.WriteResponse(ctx, nil, deverrors.FileNotFoundDevError)
		}
		return http.WriteResponse(ctx, response.Props(), nil)
	}
	return nil
}

func (instance *DevWebserver) handleProjectOpenDir(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		params, err := http.AssertParams(ctx, []string{"name"})
		if nil != err {
			return http.WriteResponse(ctx, nil, err)
		}
		name := lygo_reflect.GetString(params, "name")

		info := instance.controller.Files().GetProjectInfo(name)
		root := lygo_reflect.GetString(info, "root")
		_, err = lygo_paths.Exists(root)
		if nil == err {
			err = lygo_exec.Open(root)
		}

		return http.WriteResponse(ctx, nil == err, err)
	}
	return nil
}

func (instance *DevWebserver) handleProjectOpenVSCode(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		params, err := http.AssertParams(ctx, []string{"name"})
		if nil != err {
			return http.WriteResponse(ctx, nil, err)
		}
		name := lygo_reflect.GetString(params, "name")

		info := instance.controller.Files().GetProjectInfo(name)
		root := lygo_reflect.GetString(info, "root")
		_, err = lygo_paths.Exists(root)
		if nil == err {
			cmd := vscode.NewCommand()
			cmd.SetDir(root)
			err = cmd.Open(".")
		}

		return http.WriteResponse(ctx, nil == err, err)
	}
	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
// 	c o m p i l e r
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevWebserver) handleCompilerDeploy(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		response, err := instance.controller.Deploy()
		if nil != err {
			return http.WriteResponse(ctx, nil, err)
		}
		err = instance.guardian.Restart()
		return http.WriteResponse(ctx, response, err)
	}
	return nil
}

func (instance *DevWebserver) handleCompilerDeployUI(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		response, err := instance.controllerUI.Deploy()
		if nil != err {
			return http.WriteResponse(ctx, nil, err)
		}
		err = instance.guardian.Restart()
		return http.WriteResponse(ctx, response, err)
	}
	return nil
}

func (instance *DevWebserver) handleCompilerStartUI(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		err := instance.controllerUI.StartDev()
		return http.WriteResponse(ctx, nil == err, err)
	}
	return nil
}

func (instance *DevWebserver) handleCompilerStartedUI(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		response := instance.controllerUI.IsStartedDevSession()
		return http.WriteResponse(ctx, response, nil)
	}
	return nil
}

func (instance *DevWebserver) handleCompilerStopUI(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		err := instance.controllerUI.StopDevSession()
		return http.WriteResponse(ctx, nil == err, err)
	}
	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
// 	files ui
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevWebserver) handleReloadUI(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		return http.WriteResponse(ctx, instance.controllerUI.Files().Reload(), nil)
	}
	return nil
}

func (instance *DevWebserver) handleGetProjectsUI(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		return http.WriteResponse(ctx, instance.controllerUI.Files().GetProjects(), nil)
	}
	return nil
}

func (instance *DevWebserver) handleGetProjectsUINames(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		return http.WriteResponse(ctx, instance.controllerUI.Files().GetProjectsNames(), nil)
	}
	return nil
}

func (instance *DevWebserver) handleGetProjectUIInfo(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		params, err := http.AssertParams(ctx, []string{"name"})
		if nil != err {
			return http.WriteResponse(ctx, nil, err)
		}
		name := lygo_reflect.GetString(params, "name")
		response := instance.controllerUI.Files().GetProjectInfo(name)
		return http.WriteResponse(ctx, response, nil)
	}
	return nil
}

func (instance *DevWebserver) handleActivateProjectUI(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		params, err := http.AssertParams(ctx, []string{"name"})
		if nil != err {
			return http.WriteResponse(ctx, nil, err)
		}
		name := lygo_reflect.GetString(params, "name")
		response := instance.controllerUI.Files().ActivateProject(name)
		return http.WriteResponse(ctx, response, nil)
	}
	return nil
}

func (instance *DevWebserver) handleProjectUIOpenDir(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		params, err := http.AssertParams(ctx, []string{"name"})
		if nil != err {
			return http.WriteResponse(ctx, nil, err)
		}
		name := lygo_reflect.GetString(params, "name")

		info := instance.controllerUI.Files().GetProjectInfo(name)
		root := lygo_reflect.GetString(info, "path")
		_, err = lygo_paths.Exists(root)
		if nil == err {
			err = lygo_exec.Open(root)
		}

		return http.WriteResponse(ctx, nil == err, err)
	}
	return nil
}

func (instance *DevWebserver) handleProjectUIOpenVSCode(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		params, err := http.AssertParams(ctx, []string{"name"})
		if nil != err {
			return http.WriteResponse(ctx, nil, err)
		}
		name := lygo_reflect.GetString(params, "name")

		info := instance.controllerUI.Files().GetProjectInfo(name)
		root := lygo_reflect.GetString(info, "path")
		_, err = lygo_paths.Exists(root)
		if nil == err {
			cmd := vscode.NewCommand()
			cmd.SetDir(root)
			err = cmd.Open(".")
		}

		return http.WriteResponse(ctx, nil == err, err)
	}
	return nil
}

func (instance *DevWebserver) handleProjectUICreate(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		params, err := http.AssertParams(ctx, []string{"name"})
		if nil != err {
			return http.WriteResponse(ctx, nil, err)
		}
		name := lygo_reflect.GetString(params, "name")
		framework := lygo_reflect.GetString(params, "framework")

		response, err := instance.controllerUI.NewProject(name, framework)
		return http.WriteResponse(ctx, response, err)
	}
	return nil
}

func (instance *DevWebserver) handleProjectUIStart(ctx *fiber.Ctx) error {
	if http.AuthenticateRequest(ctx, instance.secureManager.Auth0(), instance.webserver.Settings().Map(), true) {
		params, err := http.AssertParams(ctx, []string{"name"})
		if nil != err {
			return http.WriteResponse(ctx, nil, err)
		}
		name := lygo_reflect.GetString(params, "name")

		err = instance.controllerUI.StartDevSession(name)
		return http.WriteResponse(ctx, err == nil, err)
	}
	return nil
}
