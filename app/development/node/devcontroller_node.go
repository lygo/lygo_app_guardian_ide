package node

import (
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/commons"
	"bitbucket.org/lygo/lygo_commons/lygo_exec/console_program/npm"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"strings"
)

const nodeDir = "./node_modules"

// ---------------------------------------------------------------------------------------------------------------------
//  t y p e
// ---------------------------------------------------------------------------------------------------------------------

type DevControllerNode struct {
	dirWork            string
	dirNode            string
	packageFile        string // package.json
	packageLockFile    string
	tsConfigFile       string
	tsLintFile         string
	webpackCommonsFile string
	webpackDevFile     string
	webpackProdFile    string

	npmCmd *npm.Command
}

func NewDevControllerNode(dirWorkNPM string) *DevControllerNode {
	instance := new(DevControllerNode)
	instance.dirWork = dirWorkNPM
	instance.dirNode = lygo_paths.Concat(commons.DirDev, nodeDir)
	instance.packageFile = lygo_paths.Concat(commons.DirDev, "package.json")
	instance.packageLockFile = lygo_paths.Concat(commons.DirDev, "package-lock.json")
	instance.tsConfigFile = lygo_paths.Concat(commons.DirDev, "tsconfig.json")
	instance.tsLintFile = lygo_paths.Concat(commons.DirDev, "tslint.json")
	instance.webpackCommonsFile = lygo_paths.Concat(commons.DirDev, "webpack.common.js")
	instance.webpackDevFile = lygo_paths.Concat(commons.DirDev, "webpack.dev.js")
	instance.webpackProdFile = lygo_paths.Concat(commons.DirDev, "webpack.prod.js")
	instance.npmCmd = npm.NewCommand()
	instance.npmCmd.SetDir(instance.dirWork)

	instance.init()

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//  p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevControllerNode) IsInstalled() bool {
	if b, _ := lygo_paths.Exists(instance.dirNode); b {
		if b, _ := lygo_paths.Exists(instance.packageFile); b {
			return true
		}
	}
	return false
}

func (instance *DevControllerNode) SetUp() error {
	return instance.deploy()
}

// ---------------------------------------------------------------------------------------------------------------------
//  p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevControllerNode) init() {

}

func (instance *DevControllerNode) deploy() error {
	// package.json
	content := GetPackageFileContent()
	_, err := lygo_io.WriteTextToFile(content, instance.packageFile)
	if nil != err {
		return err
	}
	_ = lygo_io.Remove(instance.packageLockFile)

	// npm install
	_, err = instance.npmCmd.Install()
	if nil != err && strings.Index(err.Error(), "package-lock.json") == -1 {
		return err
	}

	return nil
}
