package node

import (
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/commons"
	"bitbucket.org/lygo/lygo_commons/lygo_exec/console_program/npm"
)

const tplPackage = `
{
  "name": "{{.Name}}",
  "version": "{{.Version}}",
  "description": "{{.Description}}",
  "main": "{{.Main}}",
  "scripts": {
    {{.Scripts}}
  },
  "repository": {
    "type": "git",
    "url": "{{.RepositoryURL}}"
  },
  "devDependencies": {
    "ts-loader": "^5.4.5",
    "tslint": "^5.20.1",
    "typescript": "^3.9.7",
    "webpack": "^4.44.2",
    "webpack-cli": "^3.3.12",
    "webpack-merge": "^4.2.2"
  },
  "author": "{{.Author}}",
  "license": "{{.License}}"
}
`

func GetPackageFileContent() string {
	data := new(npm.DataPackage)
	data.Name = "guardian"
	data.Description = "Guardian Tool Chain v." + commons.Version
	data.Main = "main.ts"
	data.Version = commons.Version
	data.License = "MIT"
	data.RepositoryURL = "https://bitbucket.org/lygo/lygo_app_guardian_ide/src"
	data.Scripts = "\"build\": \"webpack --config webpack.prod.js\",\n    \"dev\": \"webpack --config webpack.dev.js\",\n    \"watch\": \"webpack -w --config webpack.dev.js\",\n    \"lint\": \"tslint 'lyts/**/*.{ts, tsx}'\""
	text, _ := npm.MergeTpl(tplPackage, data)
	return text
}
