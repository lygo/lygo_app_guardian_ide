package projects

import (
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/commons"
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/development/node"
	"bitbucket.org/lygo/lygo_commons/lygo_array"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"fmt"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
//  t y p e
// ---------------------------------------------------------------------------------------------------------------------

type DevController struct {
	dirWork         string
	dirWorkPrograms string
	dirDev          string
	logger          *commons.Logger
	files           *DevFiles
	node            *node.DevControllerNode
	compiler        *DevControllerCompile
	err             error // some initialization error
}

func NewDevController(l *commons.Logger) *DevController {
	instance := new(DevController)
	instance.dirWork = commons.DirWork
	instance.dirWorkPrograms = commons.DirWorkPrograms
	instance.dirDev = commons.DirDev
	instance.logger = l
	instance.files = NewDevFiles()
	instance.node = node.NewDevControllerNode(commons.DirDev) // node.js controller
	instance.compiler = NewDevControllerCompile()

	instance.err = instance.init()
	if nil != instance.err {
		instance.logger.Error("DevController.init()", instance.err)
	}

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//  p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevController) DirFiles() string {
	return instance.files.DirRoot()
}

func (instance *DevController) GetError() error {
	return instance.err
}

func (instance *DevController) Files() *DevFiles {
	return instance.files
}

func (instance *DevController) IsInstalled() bool {
	return instance.node.IsInstalled()
}

func (instance *DevController) Deploy() (map[string]interface{}, error) {
	// reload files before deploy
	instance.files.Reload()

	m := make(map[string]interface{})
	names := instance.files.GetProjectsNames()
	m["names"] = names
	deployFiles := instance.files.GetDeployableFiles()
	m["files"] = deployFiles
	deployRes := instance.files.GetResourceFiles()
	m["resources"] = deployRes

	// deploy all files and resources
	response, err := instance.compiler.Deploy(deployFiles, deployRes)
	m["log"] = response

	// deploy settings and global resources files
	settings := make([]string, 0)
	for _, prj := range names {
		info := instance.files.GetProjectFile(prj)
		if info.Active {
			settingsFiles := instance.files.GetProjectFilesDeploy(prj)
			for _, f := range settingsFiles {
				name, err := instance.deploySettingFile(prj, info, f)
				if nil == err {
					settings = append(settings, name)
				}
			}
		}
	}
	m["settings"] = settings

	return m, err
}

func (instance *DevController) NewProject(name, taskName, webhandlerName string) (*DevProject, error) {
	return instance.files.CreateProject(name, taskName, webhandlerName)
}

// ---------------------------------------------------------------------------------------------------------------------
//  p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevController) init() error {
	if !instance.node.IsInstalled() {
		// try to setup node toolchain
		err := instance.node.SetUp()
		if nil != err {
			return err
		}
	}

	if !instance.compiler.IsInstalled() {
		// try to setup node toolchain
		err := instance.compiler.SetUp()
		if nil != err {
			return err
		}
	}

	// refresh files to deploy
	err := instance.compiler.SetFilesToDeploy(instance.files.GetDeployableFiles())

	return err
}

func (instance *DevController) updateSettingJson(prjName, content string) string {
	if m, b := lygo_json.StringToMap(content); b {
		files := instance.files.GetProjectTasks(prjName)
		actions := lygo_reflect.GetArray(m, "actions")
		// loop on task files
		for _, file := range files {
			info := file.Info()
			if nil == info || nil == info.Action {
				continue // next file to process
			}
			// lookup action index and file
			idx := lygo_array.IndexOfFunc(actions, func(item interface{}) bool {
				if file.ObjectName == lygo_reflect.GetString(item, "uid") {
					return true
				}
				return false
			})
			if idx == -1 {
				// add missing item
				action := map[string]interface{}{}
				updateAction(action, prjName, file, info)
				actions = append(actions, action)
				m["actions"] = actions // update original object
			} else {
				// edit
				action := lygo_array.GetAt(actions, idx, nil).(map[string]interface{})
				updateAction(action, prjName, file, info)
			}
		}
		return lygo_json.Stringify(m)
	}
	return content
}

func (instance *DevController) updateWebserverJson(prjName, content string) string {
	if m, b := lygo_json.StringToMap(content); b {
		files := instance.files.GetProjectWebhandlers(prjName)
		routing := lygo_reflect.GetArray(m, "routing")
		for _, file := range files {
			info := file.Info()
			if nil == info || nil == info.Routing {
				continue // next file to process
			}
			// lookup route index and file
			idx := lygo_array.IndexOfFunc(routing, func(item interface{}) bool {
				if action, b := lygo_reflect.Get(item, "action").(map[string]interface{}); b {
					if file.ObjectName == lygo_reflect.GetString(action, "uid") {
						return true
					}
				}
				return false
			})
			if idx == -1 {
				// add missing item
				route := map[string]interface{}{}
				updateRoute(route, prjName, file, info)
				routing = append(routing, route)
				m["routing"] = routing // update original object
			} else {
				// edit
				route := lygo_array.GetAt(routing, idx, nil).(map[string]interface{})
				updateRoute(route, prjName, file, info)
			}
		}
		return lygo_json.Stringify(m)
	}
	return content
}

func (instance *DevController) deploySettingFile(prjName string, info DevProjectFile, file *DevFile) (string, error) {
	content, err := lygo_io.ReadTextFromFile(file.Path)
	if nil == err {
		// check file contains all webhandlers and
		if strings.ToLower(file.Name) == "settings.json" {
			content = instance.updateSettingJson(prjName, content)
		} else if strings.ToLower(file.Name) == "webserver.json" {
			content = instance.updateWebserverJson(prjName, content)
		}
		// save file
		name := lygo_paths.FileName(file.Name, false)
		ext := file.Type
		target := lygo_paths.Concat(instance.dirWork, name+"."+commons.ModeDebug+"."+ext)
		subDir := commons.GetProjectRelativePath(prjName, lygo_paths.Dir(file.Path))
		if len(subDir) > 1 {
			// this is a project resource, not a configuration file
			target = lygo_paths.Concat(instance.dirWorkPrograms, prjName, subDir, lygo_paths.FileName(file.Name, true))
		}
		// overwrite debug files
		_ = lygo_paths.Mkdir(target)
		_, _ = lygo_io.WriteTextToFile(content, target)
		// _, _ = lygo_io.WriteTextToFile(strings.Replace(content, "\"log-level\": \"debug\",", "\"log-level\": \"production\",", 1), lygo_paths.Concat(instance.dirWork, name+"."+commons.ModeProduction+"."+ext))
	}
	return file.Name, err
}

// ---------------------------------------------------------------------------------------------------------------------
//  S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func updateAction(action map[string]interface{}, prjName string, file *DevFile, info *DevFileInfo) {
	timeline := lygo_reflect.GetString(info.Action, "timeline")
	keepAlive  := lygo_reflect.GetBool(info.Action, "keep_alive")
	startAt := lygo_reflect.GetString(info.Action, "start_at")

	action["uid"] = file.ObjectName
	action["keep_alive"] = keepAlive
	action["program"] = fmt.Sprintf("%v/%v/%v/%v/%v.js",
		"./programs", prjName, "tasks", file.ObjectName, file.ObjectName) // "./programs/pap24-server/tasks/nio-srv/nio-srv.js"
	if len(timeline)>0{
		action["timeline"] = timeline
	}
	if len(startAt)>0{
		action["start_at"] = startAt
	}
}

func updateRoute(route map[string]interface{}, prjName string, file *DevFile, info *DevFileInfo) {

	// update existing route
	method := lygo_reflect.GetString(info.Routing, "method")
	if len(method) > 0 {
		route["method"] = method
	} else {
		route["method"] = "ALL"
	}
	endpoint := lygo_reflect.GetString(info.Routing, "endpoint")
	if len(endpoint) > 0 {
		route["endpoint"] = endpoint
	} else {
		route["endpoint"] = fmt.Sprintf("/api/v1/%v/%v", prjName, file.ObjectName)
	}
	// action
	var action map[string]interface{}
	if action, _ = route["action"].(map[string]interface{}); nil == action {
		action = map[string]interface{}{}
	}
	action["uid"] = file.ObjectName
	action["program"] = fmt.Sprintf("%v/%v/%v/%v/%v.js", "./programs", prjName, "webhandlers", file.ObjectName, file.ObjectName)
	action["variables"] = lygo_reflect.Get(info.Routing, "variables")
	route["action"] = action
}
