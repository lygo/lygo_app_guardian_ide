package projects

import (
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/commons"
	"bitbucket.org/lygo/lygo_commons/lygo_exec/console_program/npm"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"fmt"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
//  t y p e
// ---------------------------------------------------------------------------------------------------------------------

type DevControllerCompile struct {
	dirWork string
	//dirWorkPrograms     string
	dirDev              string
	dirBuild            string
	dirSources          string
	dirProjects         string
	tsConfigFile        string
	tsLintFile          string
	webpackCommonsFile  string
	webpackDevFile      string
	webpackProdFile     string
	typescriptTypesFile string

	npmCmd *npm.Command
}

func NewDevControllerCompile() *DevControllerCompile {
	instance := new(DevControllerCompile)
	instance.dirWork = commons.DirWork
	instance.dirDev = commons.DirDev
	instance.dirBuild = commons.DirDevBuild
	instance.dirSources = commons.DirDevSources
	instance.dirProjects = commons.DirDevProjects

	// npm configuration
	instance.tsConfigFile = lygo_paths.Concat(instance.dirDev, "tsconfig.json")
	instance.tsLintFile = lygo_paths.Concat(instance.dirDev, "tslint.json")
	instance.webpackCommonsFile = lygo_paths.Concat(instance.dirDev, "webpack.common.js")
	instance.webpackDevFile = lygo_paths.Concat(instance.dirDev, "webpack.dev.js")
	instance.webpackProdFile = lygo_paths.Concat(instance.dirDev, "webpack.prod.js")
	instance.npmCmd = npm.NewCommand()
	instance.npmCmd.SetDir(instance.dirDev)
	// sources basic files
	instance.typescriptTypesFile = lygo_paths.Concat(instance.dirSources, "runtime.d.ts")

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//  p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevControllerCompile) IsInstalled() bool {
	if b, _ := lygo_paths.Exists(instance.tsConfigFile); b {
		if b, _ := lygo_paths.Exists(instance.tsLintFile); b {
			if b, _ := lygo_paths.Exists(instance.webpackCommonsFile); b {
				if b, _ := lygo_paths.Exists(instance.webpackDevFile); b {
					if b, _ := lygo_paths.Exists(instance.webpackProdFile); b {
						if b, _ := lygo_paths.Exists(instance.typescriptTypesFile); b {
							return true
						}
					}
				}
			}
		}
	}
	return false
}

func (instance *DevControllerCompile) SetUp() error {
	return instance.init()
}

func (instance *DevControllerCompile) Deploy(projectFiles []*DevFile, resourceFiles []*DevFile) (string, error) {
	err := instance.SetFilesToDeploy(projectFiles)
	if nil != err {
		return "", err
	}
	out, err := instance.Build()
	if nil != err {
		return "", err
	}

	err = instance.copyBuildToPrograms()
	if nil != err {
		return "", err
	}

	err = instance.copyFilesToPrograms(resourceFiles)

	return out, err
}

func (instance *DevControllerCompile) SetFilesToDeploy(filesList []*DevFile) error {
	filesTxt := ""
	// add files
	// "programs/webhandlers/sys-id/sys-id": path.join(__dirname, "./sources/programs/webhandlers/sys-id/main.ts"),
	for _, file := range filesList {
		// sub := file.SubCategory
		// name := file.ObjectName
		// fname := file.Name
		// pname := file.NameInCategory
		targetPath := getTargetPath(file) // fmt.Sprintf("\"programs/%v/%v/%v/%v\"", pname, sub, name, name)
		sourcePath := getSourcePath(file)
		k := fmt.Sprintf("\"%v\"", targetPath)
		v := fmt.Sprintf("path.join(__dirname, \"%v\")", sourcePath) // fmt.Sprintf("path.join(__dirname, \"./sources/projects/%v/%v/%v/%v\")", pname, sub, name, fname)
		if len(filesTxt) > 0 {
			filesTxt += ",\n"
		}
		filesTxt += fmt.Sprintf("%v:%v", k, v)
	}
	filesTxt = "{\n" + filesTxt + "\n}"

	data := new(TplDataFiles)
	data.Files = filesTxt

	_, err := lygo_io.WriteTextToFile(GetWebpackDev(data), instance.webpackDevFile)
	if nil != err {
		return err
	}
	_, err = lygo_io.WriteTextToFile(GetWebpackProd(data), instance.webpackProdFile)
	if nil != err {
		return err
	}

	return nil
}

func (instance *DevControllerCompile) Build() (string, error) {
	return instance.npmCmd.Run("build")
}

// ---------------------------------------------------------------------------------------------------------------------
//  p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevControllerCompile) init() error {
	// tsconfig.json
	_, err := lygo_io.WriteTextToFile(tsconfig, instance.tsConfigFile)
	if nil != err {
		return err
	}
	// tslint.json
	_, err = lygo_io.WriteTextToFile(tslint, instance.tsLintFile)
	if nil != err {
		return err
	}
	// webpack.commons.json
	_, err = lygo_io.WriteTextToFile(webpackCommons, instance.webpackCommonsFile)
	if nil != err {
		return err
	}
	// runtime.d.ts
	_ = lygo_paths.Mkdir(instance.typescriptTypesFile)
	_, err = lygo_io.WriteTextToFile(TplTypescriptRuntime, instance.typescriptTypesFile)
	if nil != err {
		return err
	}

	return instance.SetFilesToDeploy([]*DevFile{})
}

func getTargetPath(file *DevFile) string {
	sub := file.SubCategory
	name := file.ObjectName
	pname := file.NameInCategory
	return fmt.Sprintf("programs/%v/%v/%v/%v", pname, sub, name, name)
}

func getSourcePath(file *DevFile) string {
	sub := file.SubCategory
	name := file.ObjectName
	fname := file.Name
	pname := file.NameInCategory
	return fmt.Sprintf("./sources/projects/%v/%v/%v/%v", pname, sub, name, fname)
}

func (instance *DevControllerCompile) copyBuildToPrograms() error {
	buildDir := instance.dirBuild
	targetDir := instance.dirWork
	files, err := lygo_paths.ListFiles(buildDir, "")
	if nil != err {
		return err
	}
	for _, source := range files {
		target := lygo_paths.Concat(targetDir, strings.ReplaceAll(source, buildDir, ""))
		_ = lygo_paths.Mkdir(target)
		_, err = lygo_io.CopyFile(source, target)
		if nil != err {
			return err
		}
	}
	return nil
}

func (instance *DevControllerCompile) copyFilesToPrograms(files []*DevFile) error {
	targetDir := instance.dirWork
	for _, file := range files {
		prj := file.NameInCategory
		subDir := commons.GetProjectRelativePath(prj, lygo_paths.Dir(file.Path))
		target := lygo_paths.Concat(targetDir, lygo_paths.Dir(getTargetPath(file)), file.Name)
		if len(subDir) > 1 {
			// this is a project resource, not a configuration file
			target = lygo_paths.Concat(commons.DirWorkPrograms, prj, subDir, lygo_paths.FileName(file.Name, true))
		}

		_ = lygo_paths.Mkdir(target)
		_, err := lygo_io.CopyFile(file.Path, target)
		if nil != err {
			return err
		}
	}
	return nil
}
