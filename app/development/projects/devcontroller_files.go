package projects

import (
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/commons"
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/development/deverrors"
	"bitbucket.org/lygo/lygo_commons/lygo_array"
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_crypto"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"fmt"
	"os"
	"strings"
)

const CAT_LIBS = commons.CAT_LIBS
const CAT_PROJECTS = commons.CAT_PROJECTS

const SUB_COMMONS = commons.SUB_COMMONS
const SUB_TASKS = commons.SUB_TASKS
const SUB_WEBHANDLERS = commons.SUB_WEBHANDLERS

const TYPE_PROJECT = commons.TYPE_PROJECT
const TYPE_RESOURCE = commons.TYPE_RESOURCE
const TYPE_DEPLOY = commons.TYPE_DEPLOY
const TYPE_UNDEFINED = commons.TYPE_UNDEFINED

var COMPILABLE_TYPES = []string{"ts"}
var PROJECT_TYPES = []string{"ts", "aql", "py", "php", "js", "rb"}

var catDirectories = make(map[string]string)

type DevProjectFile struct {
	Active  bool   `json:"active"`
	Version string `json:"version"`
}

// ---------------------------------------------------------------------------------------------------------------------
//  DevFile
// ---------------------------------------------------------------------------------------------------------------------

type DevProject struct {
	Name        string         `json:"name"`
	Path        string         `json:"path"`
	Info        DevProjectFile `json:"info"`
	Commons     []*DevProgram  `json:"commons"`
	Tasks       []*DevProgram  `json:"tasks"`
	WebHandlers []*DevProgram  `json:"webhandlers"`
}

type DevProgram struct {
	Name string       `json:"name"`
	Path string       `json:"path"`
	Info *DevFileInfo `json:"info"`
}

type DevFile struct {
	Uid            string `json:"uid"`
	Path           string `json:"path"`
	Name           string `json:"name"`        // main.ts
	SimpleName     string `json:"simple-name"` // main
	ObjectName     string `json:"object-name"` // sys-id
	ObjectType     string `json:"object-type"` // resource, project
	Type           string `json:"type"`        // .ts
	Category       string `json:"category"`    // libs, projects
	CategoryPath   string `json:"category-path"`
	RelativePath   string `json:"relative-path"`
	NameInCategory string `json:"category-name"` // project name or common lib name
	SubCategory    string `json:"sub-category"`  // commons, webhandlers, tasks

}

type DevFileProps struct {
	DevFile
	Size int64 `json:"size"`
}

type DevFileInfo struct {
	Version string                 `json:"version"`
	Action  map[string]interface{} `json:"action"`
	Routing map[string]interface{} `json:"routing"`
}

func NewDevFile(filename string) *DevFile {
	instance := new(DevFile)
	instance.Path = filename

	instance.init()

	return instance
}

func (instance *DevFile) String() string {
	return lygo_json.Stringify(instance)
}

func (instance *DevFile) GoString() string {
	return instance.String()
}

func (instance *DevFile) Props() *DevFileProps {
	response := new(DevFileProps)
	response.Path = instance.Path
	response.Name = instance.Name
	response.Uid = instance.Uid
	response.Type = instance.Type
	response.Category = instance.Category
	response.CategoryPath = instance.CategoryPath
	response.NameInCategory = instance.NameInCategory
	response.Size = instance.Size()
	return response
}

func (instance *DevFile) FileName() string {
	return lygo_paths.FileName(instance.Path, true)
}

func (instance *DevFile) FileExt() string {
	return lygo_paths.Extension(instance.Path) // .ts
}

func (instance *DevFile) Absolute() string {
	return instance.Path
}

func (instance *DevFile) Relative() string {
	return strings.ReplaceAll(instance.Path, instance.CategoryPath, "./")
}

func (instance *DevFile) Size() int64 {
	var b bool
	if b, _ = lygo_paths.Exists(instance.Path); b {
		s, _ := lygo_io.FileSize(instance.Path)
		return s
	}
	return 0
}

func (instance *DevFile) IsDeployable() bool {
	return isDeployable(instance)
}

func (instance *DevFile) Info() *DevFileInfo {
	if instance.IsDeployable() {
		info := new(DevFileInfo)
		path := lygo_paths.ChangeFileNameExtension(instance.Path, ".json")
		getOrCreateInfoFile(path, info)
		return info
	}
	return nil
}

func (instance *DevFile) Remove() (err error) {
	if _, err = lygo_paths.Exists(instance.Path); nil == err {
		err = lygo_io.Remove(instance.Path)
	}
	return
}

func (instance *DevFile) Read() (text string, err error) {
	var b bool
	if b, err = lygo_paths.Exists(instance.Path); b {
		text, err = lygo_io.ReadTextFromFile(instance.Path)
	}
	return
}

func (instance *DevFile) Write(text string) (err error) {
	var b bool
	if b, err = lygo_paths.Exists(instance.Path); b {
		_, err = lygo_io.WriteTextToFile(text, instance.Path)
	}
	return
}

func (instance *DevFile) Move(targetDir string) (err error) {
	var b bool
	if b, err = lygo_paths.Exists(targetDir); b {
		target := lygo_paths.Concat(targetDir, instance.FileName())
		_, err = lygo_io.CopyFile(instance.Path, target)
		if nil == err {
			// remove old file
			rmErr := lygo_io.Remove(instance.Path)
			if nil != rmErr {
				// rollback
				err = rmErr
				_ = lygo_io.Remove(target)
			} else {
				instance.Path = target
				instance.init() // reload category
			}
		}
	}
	return
}

func (instance *DevFile) Rename(targetName string) (err error) {
	var b bool
	if b, err = lygo_paths.Exists(targetName); !b {
		_, err = lygo_io.CopyFile(instance.Path, targetName)
		if nil == err {
			// remove old file
			rmErr := lygo_io.Remove(instance.Path)
			if nil != rmErr {
				// rollback
				err = rmErr
				_ = lygo_io.Remove(targetName)
			} else {
				instance.Path = targetName
				instance.init() // reload category
			}
		}
	} else {
		return deverrors.FileExistsDevError
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevFile) init() {
	instance.Uid = lygo_crypto.MD5(instance.Path)
	instance.Name = lygo_paths.FileName(instance.Path, true)
	instance.SimpleName = lygo_paths.FileName(instance.Path, false)
	instance.Type = strings.ToLower(lygo_paths.ExtensionName(instance.Path))
	instance.Category, instance.CategoryPath = parsePath(instance.Path)
	instance.RelativePath = "." + strings.ReplaceAll(instance.Path, instance.CategoryPath, "")

	tokens := strings.Split(instance.Path, string(os.PathSeparator))
	categoryIndex := lygo_array.IndexOf(instance.Category, tokens)
	if categoryIndex > -1 {
		instance.NameInCategory = tokens[categoryIndex+1]
		instance.ObjectName = instance.NameInCategory
		if instance.Category == CAT_PROJECTS {
			instance.SubCategory = lygo_array.GetAt(tokens, categoryIndex+2, "").(string)
			instance.ObjectName = lygo_array.GetAt(tokens, categoryIndex+3, "").(string)
		}
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//  DevFiles
// ---------------------------------------------------------------------------------------------------------------------

type DevFiles struct {
	dirWork     string
	dirRoot     string // development root
	dirBuild    string
	dirSources  string
	dirProjects string
	dirLibs     string

	files []*DevFile
}

func NewDevFiles() *DevFiles {
	instance := new(DevFiles)
	instance.dirWork = commons.DirWork
	instance.dirRoot = commons.DirDev             //lygo_paths.Concat(dirWork, commons.DirDevelopment)
	instance.dirBuild = commons.DirDevBuild       //lygo_paths.Concat(instance.dirRoot, "build")
	instance.dirSources = commons.DirDevSources   //lygo_paths.Concat(instance.dirRoot, "sources")
	instance.dirLibs = commons.DirDevLibs         // lygo_paths.Concat(instance.dirSources, CAT_LIBS)
	instance.dirProjects = commons.DirDevProjects // lygo_paths.Concat(instance.dirSources, CAT_PROJECTS)

	instance.files = make([]*DevFile, 0)

	instance.init()

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//  p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevFiles) DirRoot() string {
	return instance.dirRoot
}

func (instance *DevFiles) AddProjectCommon(name string) error {
	filename := lygo_paths.Concat(instance.dirProjects, "commons", name, name+".ts")
	return createDevFile(filename, false) // not deployed
}

func (instance *DevFiles) AddProjectTask(name string) error {
	filename := lygo_paths.Concat(instance.dirProjects, "tasks", name, name+".ts")
	return createDevFile(filename, true)
}

func (instance *DevFiles) AddProjectWebHandler(name string) error {
	filename := lygo_paths.Concat(instance.dirProjects, "webhandlers", name, name+".ts")
	return createDevFile(filename, true)
}

func (instance *DevFiles) AddFile(absolutePath string) (*DevFile, error) {
	_ = lygo_paths.Mkdir(absolutePath)
	if b, _ := lygo_paths.Exists(absolutePath); !b {
		_, err := lygo_io.WriteTextToFile("", absolutePath)
		if nil != err {
			return nil, err
		}
	}
	file := NewDevFile(absolutePath)
	instance.files = append(instance.files, file)
	return file, nil
}

func (instance *DevFiles) RemoveFile(absolutePath string) error {
	idx := indexOf(absolutePath, instance.files)
	if idx > -1 {
		file := instance.files[idx]
		err := file.Remove()
		if nil != err {
			return err
		}
		instance.files = append(instance.files[:idx], instance.files[idx+1:]...)
	}
	return nil
}

func (instance *DevFiles) Reload() []*DevFile {
	instance.loadFiles()
	return instance.files
}

func (instance *DevFiles) GetOrCreate(path string) (*DevFile, error) {
	response := instance.GetByPath(path)
	if nil == response {
		// validate path
		err := validatePath(path)
		if nil != err {
			return nil, err
		}
		return instance.AddFile(path)
	}
	return response, nil
}

func (instance *DevFiles) Get(id string) *DevFile {
	response := instance.GetByUid(id)
	if nil == response {
		response = instance.GetByPath(id)
	}
	return response
}

func (instance *DevFiles) GetByPath(path string) *DevFile {
	for _, file := range instance.files {
		if file.Path == path {
			return file
		}
	}
	return nil
}

func (instance *DevFiles) GetByUid(uid string) *DevFile {
	for _, file := range instance.files {
		if file.Uid == uid {
			return file
		}
	}
	return nil
}

func (instance *DevFiles) GetByCategory(category string) []*DevFile {
	response := make([]*DevFile, 0)
	for _, file := range instance.files {
		if file.Category == category {
			response = append(response, file)
		}
	}
	return response
}

func (instance *DevFiles) GetLibs() []*DevFile {
	return instance.GetByCategory(CAT_LIBS)
}

func (instance *DevFiles) GetProjects() []*DevFile {
	instance.Reload()
	return instance.GetByCategory(CAT_PROJECTS)
}

func (instance *DevFiles) GetNamesInCategory(category string) []string {
	response := make([]string, 0)
	files := instance.GetByCategory(category)
	for _, file := range files {
		if lygo_array.IndexOf(file.NameInCategory, response) == -1 {
			response = append(response, file.NameInCategory)
		}
	}
	return response
}

func (instance *DevFiles) GetProjectsNames() []string {
	return instance.GetNamesInCategory(CAT_PROJECTS)
}

func (instance *DevFiles) GetLibsNames() []string {
	return instance.GetNamesInCategory(CAT_LIBS)
}

// return files for each project or lib
func (instance *DevFiles) GetFilesForCategoryObject(category, nameInCategory, objectType string) []*DevFile {
	response := make([]*DevFile, 0)
	files := instance.GetByCategory(category)
	for _, file := range files {
		if file.NameInCategory == nameInCategory {
			if len(objectType) > 0 {
				if file.ObjectType == objectType {
					response = append(response, file)
				}
			} else {
				response = append(response, file)
			}
		}
	}
	return response
}

func (instance *DevFiles) GetLibFiles(name string) []*DevFile {
	return instance.GetFilesForCategoryObject(CAT_LIBS, name, "")
}

func (instance *DevFiles) GetProjectFile(name string) DevProjectFile {
	filename := commons.GetProjectMainFileName(name)
	if b, _ := lygo_paths.Exists(filename); !b {
		m := make(map[string]interface{})
		m["active"] = false
		m["version"] = "1.0.0"
		_, _ = lygo_io.WriteTextToFile(lygo_json.Stringify(m), filename)
	}
	var response DevProjectFile
	_ = lygo_json.ReadFromFile(filename, &response)
	return response
}

func (instance *DevFiles) GetProjectInfo(name string) map[string]interface{} {
	response := make(map[string]interface{})
	response["name"] = name
	response["root"] = lygo_paths.Concat(instance.dirProjects, name)
	response["info_file"] = commons.GetProjectMainFileName(name)
	response["info"] = lygo_conv.ToMap(instance.GetProjectFile(name))

	// all files
	allFiles := instance.GetProjectFiles(name)

	// settings:
	response["settings"] = getResourceFiles(allFiles)

	// deployable: programs into projects that can be deployed as tasks or webhandlers
	deployables := getDeployableFiles(allFiles)
	programs := make([]map[string]interface{}, 0)
	for _, f := range deployables {
		program := make(map[string]interface{})
		program["name"] = f.ObjectName
		program["root"] = lygo_paths.Dir(f.Path)
		program["files"] = make([]*DevFile, 0)
		program["info_file"] = lygo_paths.Concat(lygo_paths.Dir(f.Path), "main.json")
		m, err := lygo_json.ReadMapFromFile(program["info_file"].(string))
		if nil == err {
			program["info"] = m
		}
		programs = append(programs, program)
	}
	response["programs"] = programs

	// add files to programs
	for _, f := range allFiles {
		for _, program := range programs {
			files := program["files"].([]*DevFile)
			if program["name"] == f.ObjectName {
				files = append(files, f)
			}
			program["files"] = files
		}
	}
	return response
}

func (instance *DevFiles) ActivateProject(projectName string) map[string]map[string]interface{} {
	response := make(map[string]map[string]interface{}, 0)
	// reload all
	instance.loadFiles()

	names := instance.GetProjectsNames()
	for _, name := range names {
		info := instance.GetProjectFile(name)
		if name == projectName {
			info.Active = true
		} else {
			info.Active = false
		}
		_, _ = lygo_io.WriteTextToFile(lygo_json.Stringify(info), commons.GetProjectMainFileName(name))
		response[name] = lygo_conv.ToMap(info)
	}
	return response
}

func (instance *DevFiles) GetProjectFiles(name string) []*DevFile {
	return instance.GetFilesForCategoryObject(CAT_PROJECTS, name, "")
}

func (instance *DevFiles) GetProjectFilesDeploy(name string) []*DevFile {
	return instance.GetFilesForCategoryObject(CAT_PROJECTS, name, TYPE_DEPLOY)
}

func (instance *DevFiles) GetProjectTasks(name string) []*DevFile {
	response := make([]*DevFile, 0)
	files := instance.GetByCategory(CAT_PROJECTS)
	for _, file := range files {
		if file.NameInCategory == name && file.SubCategory == SUB_TASKS {
			response = append(response, file)
		}
	}
	return response
}

func (instance *DevFiles) GetProjectWebhandlers(name string) []*DevFile {
	response := make([]*DevFile, 0)
	files := instance.GetByCategory(CAT_PROJECTS)
	for _, file := range files {
		if file.NameInCategory == name && file.SubCategory == SUB_WEBHANDLERS {
			response = append(response, file)
		}
	}
	return response
}

func (instance *DevFiles) CreateProject(name, taskName, webhandlerName string) (*DevProject, error) {
	slugName := strings.ToLower(lygo_strings.Slugify(name))
	path := lygo_paths.Dir(commons.GetProjectMainFileName(slugName))
	if b, _ := lygo_paths.Exists(path); b {
		// project exists
		return nil, commons.ProjectAlreadyExistsError
	}

	err := instance.createProjectFolders(slugName)
	if nil != err {
		return nil, err
	}

	if len(taskName) > 0 {
		dir := lygo_paths.Concat(instance.getDirProjectTasks(slugName), taskName)
		err = lygo_paths.Mkdir(lygo_paths.ConcatDir(dir))
		if nil == err {
			_, _ = lygo_io.WriteTextToFile("", lygo_paths.Concat(dir, "main.ts"))
		}
		// main.json
		filename := lygo_paths.Concat(dir, "main.json")
		info := NewDevFileInfo(filename)
		action := map[string]interface{}{
			"timeline": "second:1",
			"keep_alive": false,
		}
		info.Action = action
		_, _ = lygo_io.WriteTextToFile(lygo_json.Stringify(info), filename)
	}

	if len(webhandlerName) > 0 {
		dir := lygo_paths.Concat(instance.getDirProjectWebhandlers(slugName), webhandlerName)
		err = lygo_paths.Mkdir(lygo_paths.ConcatDir(dir))
		if nil == err {
			_, _ = lygo_io.WriteTextToFile("", lygo_paths.Concat(dir, "main.ts"))
		}
		// main.json
		filename := lygo_paths.Concat(dir, "main.json")
		info := NewDevFileInfo(filename)
		route := map[string]interface{}{
			"method":    "ALL",
			"endpoint":  fmt.Sprintf("/api/%v/v1/%v", slugName, strings.ToLower(lygo_strings.Slugify(webhandlerName))),
			"variables": map[string]interface{}{},
		}
		info.Routing = route
		_, _ = lygo_io.WriteTextToFile(lygo_json.Stringify(info), filename)
	}

	instance.Reload()

	project := instance.GetProject(name)

	return project, nil
}

func (instance *DevFiles) GetProject(name string) *DevProject {
	response := new(DevProject)
	response.Name = name
	response.Path = lygo_paths.Dir(commons.GetProjectMainFileName(name))
	response.Info = instance.GetProjectFile(name)

	// commons
	pathCommons := lygo_paths.Concat(response.Path, commons.SUB_COMMONS)
	if b, _ := lygo_paths.Exists(pathCommons); b {
		dirs, _ := lygo_paths.ReadDirOnly(pathCommons)
		programs := make([]*DevProgram, 0)
		for _, dir := range dirs {
			program := new(DevProgram)
			program.Name = lygo_paths.FileName(dir, false)
			program.Path = lygo_paths.Concat(pathCommons, dir)
			programs = append(programs, program)
		}
		response.Commons = programs
	} else {
		response.Commons = make([]*DevProgram, 0)
	}

	// tasks
	pathTasks := lygo_paths.Concat(response.Path, commons.SUB_TASKS)
	if b, _ := lygo_paths.Exists(pathTasks); b {
		dirs, _ := lygo_paths.ReadDirOnly(pathTasks)
		programs := make([]*DevProgram, 0)
		for _, dir := range dirs {
			program := new(DevProgram)
			program.Name = dir
			program.Path = lygo_paths.Concat(pathTasks, dir)
			program.Info = NewDevFileInfo(lygo_paths.Concat(program.Path, "main.json"))
			programs = append(programs, program)
		}
		response.Tasks = programs
	} else {
		response.Tasks = make([]*DevProgram, 0)
	}

	// webhandlers
	pathWebhandlers := lygo_paths.Concat(response.Path, commons.SUB_WEBHANDLERS)
	if b, _ := lygo_paths.Exists(pathWebhandlers); b {
		dirs, _ := lygo_paths.ReadDirOnly(pathWebhandlers)
		programs := make([]*DevProgram, 0)
		for _, dir := range dirs {
			program := new(DevProgram)
			program.Name = dir
			program.Path = lygo_paths.Concat(pathWebhandlers, dir)
			program.Info = NewDevFileInfo(lygo_paths.Concat(program.Path, "main.json"))
			programs = append(programs, program)
		}
		response.WebHandlers = programs
	} else {
		response.WebHandlers = make([]*DevProgram, 0)
	}

	return response
}

func (instance *DevFiles) GetDeployableFiles() []*DevFile {
	return getDeployableFiles(instance.files)
}

func (instance *DevFiles) GetResourceFiles() []*DevFile {
	return getResourceFiles(instance.files)
}

// ---------------------------------------------------------------------------------------------------------------------
//  p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevFiles) init() {
	// paths
	_ = lygo_paths.Mkdir(instance.dirRoot + string(os.PathSeparator))
	_ = lygo_paths.Mkdir(instance.dirBuild + string(os.PathSeparator))
	_ = lygo_paths.Mkdir(instance.dirSources + string(os.PathSeparator))

	// source files
	catDirectories[CAT_LIBS] = lygo_paths.Concat(instance.dirSources, CAT_LIBS)
	catDirectories[CAT_PROJECTS] = lygo_paths.Concat(instance.dirSources, CAT_PROJECTS)
	for _, v := range catDirectories {
		_ = lygo_paths.Mkdir(v + string(os.PathSeparator))
	}

	instance.loadFiles()
}

func (instance *DevFiles) loadFiles() {
	// reset
	instance.files = make([]*DevFile, 0)

	// libs
	list, err := lygo_paths.ListFiles(catDirectories[CAT_LIBS], "")
	if nil == err {
		for _, path := range list {
			instance.files = append(instance.files, NewDevFile(path))
		}
	}
	// projects
	list, err = lygo_paths.ListFiles(catDirectories[CAT_PROJECTS], "")
	if nil == err {
		for _, path := range list {
			if b, f := isLoadableFile(path); b {
				isRes := isResource(f)
				isTypeProject := isProjectType(f.Type)
				isTypeResource := isResourceType(f.Type)
				if isTypeProject {
					f.ObjectType = TYPE_PROJECT
				} else if isRes {
					f.ObjectType = TYPE_RESOURCE
				} else if isTypeResource {
					f.ObjectType = TYPE_DEPLOY
				} else {
					f.ObjectType = TYPE_UNDEFINED
				}
				instance.files = append(instance.files, f)
			}
		}
	}
}

func (instance *DevFiles) getDirProject(name string) string {
	return lygo_paths.Concat(instance.dirProjects, name)
}

func (instance *DevFiles) getDirProjectCommons(name string) string {
	return lygo_paths.Concat(instance.getDirProject(name), SUB_COMMONS)
}

func (instance *DevFiles) getDirProjectTasks(name string) string {
	return lygo_paths.Concat(instance.getDirProject(name), SUB_TASKS)
}

func (instance *DevFiles) getDirProjectWebhandlers(name string) string {
	return lygo_paths.Concat(instance.getDirProject(name), SUB_WEBHANDLERS)
}

func (instance *DevFiles) createProjectFolders(name string) error {
	dir := lygo_paths.Concat(instance.dirProjects, name)
	err := lygo_paths.Mkdir(lygo_paths.Concat(dir, SUB_COMMONS) + lygo_paths.OS_PATH_SEPARATOR)
	if nil != err {
		return err
	}
	err = lygo_paths.Mkdir(lygo_paths.Concat(dir, SUB_TASKS) + lygo_paths.OS_PATH_SEPARATOR)
	if nil != err {
		return err
	}
	err = lygo_paths.Mkdir(lygo_paths.Concat(dir, SUB_WEBHANDLERS) + lygo_paths.OS_PATH_SEPARATOR)
	if nil != err {
		return err
	}
	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
//  S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func NewProjectFile(filename string) *DevProjectFile {
	if b, _ := lygo_paths.Exists(filename); !b {
		m := make(map[string]interface{})
		m["active"] = false
		m["version"] = "1.0.0"
		_, _ = lygo_io.WriteTextToFile(lygo_json.Stringify(m), filename)
	}
	var response DevProjectFile
	_ = lygo_json.ReadFromFile(filename, &response)
	return &response
}

func NewDevFileInfo(filename string) *DevFileInfo {
	if b, _ := lygo_paths.Exists(filename); !b {
		m := make(map[string]interface{})
		m["version"] = "1.0.0"
		m["action"] = map[string]interface{}{}
		m["routing"] = map[string]interface{}{}
		_, _ = lygo_io.WriteTextToFile(lygo_json.Stringify(m), filename)
	}
	var response DevFileInfo
	_ = lygo_json.ReadFromFile(filename, &response)
	return &response
}

func indexOf(path string, files []*DevFile) int {
	for i, file := range files {
		if file.Path == path {
			return i
		}
	}
	return -1
}

func parsePath(name string) (category, categoryPath string) {
	for k, v := range catDirectories {
		if strings.Index(name, v) > -1 {
			category = k
			categoryPath = v
			return
		}
	}
	return
}

func validatePath(name string) error {
	c, _ := parsePath(name)
	if len(c) == 0 {
		return deverrors.PathNotAllowedDevError
	}
	return nil
}

func createDevFile(filename string, includeInfoFile bool) error {

	err := lygo_paths.Mkdir(filename)
	if nil != err {
		return err
	}
	_, err = lygo_io.WriteTextToFile("", filename)
	if nil != err {
		return err
	}

	if includeInfoFile {
		infoname := lygo_paths.ChangeFileNameExtension(filename, ".json")
		_, err = lygo_io.WriteTextToFile("", infoname)
		if nil != err {
			return err
		}
	}
	return nil
}

func getResourceFiles(files []*DevFile) []*DevFile {
	response := make([]*DevFile, 0)
	for _, file := range files {
		if isResource(file) {
			response = append(response, file)
		}
	}
	return response
}

func getDeployableFiles(files []*DevFile) []*DevFile {
	response := make([]*DevFile, 0)
	for _, file := range files {
		if isDeployable(file) {
			response = append(response, file)
		}
	}
	return response
}

func isLoadableFile(path string) (bool, *DevFile) {
	file := NewDevFile(path)
	if file.Name != "main.json" && strings.Index(file.Name, ".") != 0 {
		return true, file
	}
	return false, nil
}

func isDeployable(file *DevFile) bool {
	if nil != file {
		switch file.SubCategory {
		case "tasks", "webhandlers":
			if file.SimpleName == "main" {
				return true // len(file.Version) > 0
			}
		}
	}
	return false
}

func isResource(file *DevFile) bool {
	if nil != file {
		fname := file.SimpleName
		ftype := file.Type
		switch file.SubCategory {
		case "tasks", "webhandlers":
			if (fname != "main" && ftype == "json") || isResourceType(ftype) {
				return true // len(file.Version) > 0
			}
		}
	}
	return false
}

func isResourceType(t string) bool {
	return lygo_array.IndexOf(t, COMPILABLE_TYPES) == -1
}

func isProjectType(t string) bool {
	return lygo_array.IndexOf(t, PROJECT_TYPES) > -1
}

func getOrCreateInfoFile(filename string, item *DevFileInfo) {
	err := lygo_json.ReadFromFile(filename, item)
	if nil != err || len(item.Version) == 0 {
		item.Version = "1.0.0"
		item.Action = map[string]interface{}{}
		item.Routing = map[string]interface{}{}
		_, _ = lygo_io.WriteTextToFile(lygo_json.Stringify(item), filename)
	}
}
