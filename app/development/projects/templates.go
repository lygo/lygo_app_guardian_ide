package projects

import (
	"bytes"
	"strings"
	"text/template"
)

type TplDataFiles struct {
	Files string
}

const tsconfig = `
{
  "compilerOptions": {
    "outDir": "./.tmp/",
    "noImplicitAny": true,
    "module": "es6",
    "target": "es5",
    "jsx": "react",
    "allowJs": true,
    "noEmitOnError": true,
    "strictNullChecks": true,
    "sourceMap": true,
    "lib": ["dom", "es2015.promise", "es5"]
  },
  "exclude": [
    "node-modules"
  ]
}
`

const tslint = `
{
  "defaultSeverity": "error",
  "extends": [
    "tslint:recommended"
  ],
  "jsRules": {},
  "rules": {
	"no-console": false
  },
  "rulesDirectory": []
}
`

const webpackCommons = `
const path = require("path");

module.exports = {
    entry: {},
    output: {
        path: path.join(__dirname, "build"),
        filename: (context) => {
            return "[name].js";
        }
    },
    plugins: [],
    module: {
        rules: [
            {
                exclude: /node_modules/,
                use: "ts-loader"
            },
        ]
    },
    resolve: {
        extensions: [".ts", ".js"]
    },
};
`

const webpackDev = `
const path = require("path");
const merge = require("webpack-merge");
const common = require("./webpack.common.js");

module.exports = merge(common, {
    mode: "development",
    entry: {{.Files}},
    devtool: "source-map"
});
`

const webpackProd = `
const path = require("path");
const merge = require("webpack-merge");
const common = require("./webpack.common.js");

module.exports = merge(common, {
    mode: "production",
    entry: {{.Files}},
    optimization: {
        minimize: true,
    },
});
`

const TplTypescriptRuntime = `
interface ScriptingRequireFunction {
    /* tslint:disable-next-line:callable-types */
    (id: string): any;
}
declare var require: ScriptingRequireFunction;
declare var use: ScriptingRequireFunction;

declare var runtime:any;
declare var _global:any;
`

func GetWebpackDev(data *TplDataFiles) string {
	text, _ := MergeTpl(webpackDev, data)
	return text
}

func GetWebpackProd(data *TplDataFiles) string {
	text, _ := MergeTpl(webpackProd, data)
	return text
}

func MergeTpl(text string, data *TplDataFiles) (string, error) {
	buff := bytes.NewBufferString("")
	t := template.Must(template.New("template").Parse(strings.Trim(text, "\n")))
	err := t.Execute(buff, data)
	if nil != err {
		return "", err
	}
	return buff.String(), err
}
