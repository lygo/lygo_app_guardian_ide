package projects_ui

import (
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/commons"
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/development/deverrors"
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/development/node"
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"bitbucket.org/lygo/lygo_commons/lygo_exec/console_program/environment"
	"bitbucket.org/lygo/lygo_commons/lygo_exec/console_program/npm"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"errors"
	"fmt"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
//  t y p e
// ---------------------------------------------------------------------------------------------------------------------

type DevUIController struct {
	dirWWW string
	logger *commons.Logger
	node   *node.DevControllerNode
	err    error // some initialization error

	files        *DevUIFiles
	debugSession *environment.ConsoleProgramSession // react development session
}

func NewDevUIController(dirWWW string, l *commons.Logger) *DevUIController {
	instance := new(DevUIController)
	instance.dirWWW = dirWWW
	instance.logger = l
	instance.files = NewDevUIFiles()

	if nil != instance.err {
		instance.logger.Error("DevUIController.init()", instance.err)
	}

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//  p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevUIController) Files() *DevUIFiles {
	return instance.files
}

func (instance *DevUIController) Deploy() (map[string]interface{}, error) {
	response := make(map[string]interface{})

	// reload files before deploy
	instance.files.Reload()

	// get active project
	project := instance.files.GetActiveProject()
	if nil != project {
		// build the project
		out, err := instance.build(project)
		response["log"] = out
		if nil != err {
			return response, err
		}

		buildDir := lygo_paths.Concat(project.Path, "build")
		files, err := instance.copyBuildToTarget(buildDir, instance.dirWWW)
		if nil != err {
			return response, err
		}
		response["files"] = files
		response["target"] = instance.dirWWW

		// write ui.debug.json
		uiSettingsFile := lygo_paths.Concat(commons.DirWork, fmt.Sprintf("ui.%v.json", "debug"))
		m, err := lygo_json.ReadMapFromFile(uiSettingsFile)
		if nil != err {
			return nil, err
		}
		m["enabled"] = true
		_, err = lygo_io.WriteTextToFile(lygo_json.Stringify(m), uiSettingsFile)
		return response, err
	}

	return response, nil
}

func (instance *DevUIController) Exists(uid string) bool {
	info := instance.files.GetProjectInfo(uid)
	if nil != info {
		return len(info.Uid) > 0
	}
	return false
}

func (instance *DevUIController) IsStartedDevSession() bool {
	return instance.debugSession != nil
}

func (instance *DevUIController) StartDev() error {
	// reload files before deploy
	instance.files.Reload()

	// get active project
	project := instance.files.GetActiveProject()
	if nil != project {
		return instance.StartDevSession(project.Uid)
	}

	return nil
}

func (instance *DevUIController) StartDevSession(uid string) error {
	if !instance.Exists(uid) {
		return deverrors.FileNotFoundDevError
	}
	creator := NewDevUIControllerProjectCreator(instance.logger)
	session, err := creator.Start(uid)
	if nil != err {
		return err
	}

	// save session
	instance.debugSession = session

	return nil
}

func (instance *DevUIController) StopDevSession() error {
	if nil != instance.debugSession {
		err := instance.debugSession.Kill()
		instance.debugSession = nil
		if nil!=err && strings.Index(err.Error(), "finished") == -1 {
			return err
		}
	}
	return nil
}

func (instance *DevUIController) NewProject(uid, framework string) (*DevUIProject, error) {
	if instance.Exists(uid) {
		response := instance.files.GetProjectInfo(uid)
		return response, deverrors.FileExistsDevError
	}
	creator := NewDevUIControllerProjectCreator(instance.logger)
	err := creator.Create(uid, framework)
	if nil != err {
		return nil, err
	}

	// reload files before deploy
	instance.files.Reload()
	response := instance.files.GetProjectInfo(uid)
	return response, nil
}

// ---------------------------------------------------------------------------------------------------------------------
//  p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevUIController) build(project *DevUIProject) (string, error) {
	cmd := npm.NewCommand()
	cmd.SetDir(project.Path)
	return cmd.Run("build")
}

func (instance *DevUIController) copyBuildToTarget(from, to string) ([]string, error) {
	if b, err := lygo_paths.Exists(from); !b {
		if nil != err {
			return []string{}, err
		}
		return []string{}, lygo_errors.Prefix(errors.New(from), deverrors.FileNotFoundDevError.Error())
	}
	err := lygo_paths.Mkdir(to + lygo_paths.OS_PATH_SEPARATOR)
	if nil != err {
		return []string{}, err
	}

	files, err := lygo_paths.ListFiles(from, "")
	if nil != err {
		return []string{}, err
	}
	for _, source := range files {
		target := lygo_paths.Concat(to, strings.ReplaceAll(source, from, ""))
		_ = lygo_paths.Mkdir(target)
		_, err = lygo_io.CopyFile(source, target)
		if nil != err {
			return []string{}, err
		}
	}

	return files, nil
}
