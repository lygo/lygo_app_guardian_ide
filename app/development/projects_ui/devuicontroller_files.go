package projects_ui

import (
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/commons"
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"io/ioutil"
	"os"
)

type DevUIProject struct {
	Uid      string         `json:"uid"`
	Path     string         `json:"path"`
	InfoFile string         `json:"info-file"`
	Info     *DevUIInfoFile `json:"info"`
}

type DevUIFiles struct {
	dirRoot  string
	projects []*DevUIProject
}

type DevUIInfoFile struct {
	Active       bool   `json:"active"`
	Version      string `json:"version"`
	DeployTarget string `json:"deploy-target"`
}

func NewDevUIFiles() *DevUIFiles {
	instance := new(DevUIFiles)
	instance.dirRoot = commons.DirDevUI

	instance.init()

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//  p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevUIFiles) Reload() []*DevUIProject {
	_ = instance.loadFiles()
	return instance.projects
}

func (instance *DevUIFiles) GetProjects() []*DevUIProject {
	return instance.projects
}

func (instance *DevUIFiles) GetProjectsNames() []string {
	response := make([]string, 0)
	if nil != instance {
		for _, project := range instance.projects {
			response = append(response, project.Uid)
		}
	}
	return response
}

func (instance *DevUIFiles) GetProjectInfo(uid string) *DevUIProject {
	for _, project := range instance.projects {
		if uid == project.Uid {
			return project
		}
	}
	return nil
}

func (instance *DevUIFiles) GetActiveProject() *DevUIProject {
	names := instance.GetProjectsNames()
	for _, name := range names {
		project := instance.GetProjectInfo(name)
		if nil != project {
			info := project.Info
			if info.Active {
				return project
			}
		}
	}
	return nil
}

func (instance *DevUIFiles) ActivateProject(uid string) map[string]map[string]interface{} {
	response := make(map[string]map[string]interface{}, 0)
	// reload all
	_ = instance.loadFiles()

	names := instance.GetProjectsNames()
	for _, name := range names {
		project := instance.GetProjectInfo(name)
		if nil != project {
			info := project.Info
			if name == uid {
				info.Active = true
			} else {
				info.Active = false
			}
			_, _ = lygo_io.WriteTextToFile(lygo_json.Stringify(info), project.InfoFile)
			response[name] = lygo_conv.ToMap(info)
		}
	}

	return response
}

// ---------------------------------------------------------------------------------------------------------------------
//  p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevUIFiles) init() {
	// paths
	_ = lygo_paths.Mkdir(instance.dirRoot + string(os.PathSeparator))

	_ = instance.loadFiles()
}

func (instance *DevUIFiles) loadFiles() error {
	instance.projects = make([]*DevUIProject, 0)
	dirs, err := ioutil.ReadDir(instance.dirRoot)
	if nil == err {
		for _, dir := range dirs {
			if dir.IsDir() {
				item := new(DevUIProject)
				item.Uid = dir.Name()
				item.Path = lygo_paths.Concat(instance.dirRoot, dir.Name())
				item.InfoFile = lygo_paths.Concat(instance.dirRoot, dir.Name(), "main.json")
				item.Info = new(DevUIInfoFile)
				getOrCreateInfoFile(item.InfoFile, item.Info)
				instance.projects = append(instance.projects, item)
			}
		}
	}
	return err
}

func getOrCreateInfoFile(filename string, item *DevUIInfoFile) {
	err := lygo_json.ReadFromFile(filename, item)
	if nil != err || len(item.Version) == 0 {
		item.Active = false
		item.Version = "1.0.0"
		item.DeployTarget = "./"
		_, _ = lygo_io.WriteTextToFile(lygo_json.Stringify(item), filename)
	}
}
