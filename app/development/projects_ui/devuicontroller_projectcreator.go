package projects_ui

import (
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/commons"
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/development/deverrors"
	"bitbucket.org/lygo/lygo_commons/lygo_exec/console_program/environment"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
)

const (
	frameworkREACT       = "react"
	frameworkREACT_REDUX = "react-redux"
)

type DevUIControllerProjectCreator struct {
	dirRoot   string
	framework string
	logger    *commons.Logger
}

func NewDevUIControllerProjectCreator(l *commons.Logger) *DevUIControllerProjectCreator {
	instance := new(DevUIControllerProjectCreator)
	instance.dirRoot = commons.DirDevUI
	instance.logger = l

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//  p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevUIControllerProjectCreator) Start(uid string) (*environment.ConsoleProgramSession, error) {
	dir := lygo_paths.Concat(commons.DirDevUI, uid)
	if b, _ := lygo_paths.Exists(dir); !b {
		return nil, deverrors.FileNotFoundDevError
	}
	return instance.startReact(dir)
}

func (instance *DevUIControllerProjectCreator) Create(uid, framework string) (err error) {
	dir := lygo_paths.Concat(commons.DirDevUI, uid)
	if b, _ := lygo_paths.Exists(dir); b {
		return deverrors.FileExistsDevError
	}

	// creates target dir
	err = lygo_paths.Mkdir(dir + lygo_paths.OS_PATH_SEPARATOR)
	if nil != err {
		return err
	}

	switch framework {
	case frameworkREACT:
		err = instance.createReact(dir)
	case frameworkREACT_REDUX:
		err = instance.createReactRedux(dir)
	default:
		err = instance.createReact(dir)
	}

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *DevUIControllerProjectCreator) startReact(dir string) (*environment.ConsoleProgramSession, error) {
	cmd := environment.NewConsoleProgramWithDir("npm", dir)
	session, err := cmd.RunAsync("start")
	if nil != err {
		return nil, err
	}

	return session, nil
}

func (instance *DevUIControllerProjectCreator) createReact(dir string) (err error) {
	name := lygo_paths.FileName(dir, false)
	cmd := environment.NewConsoleProgramWithDir("npx", lygo_paths.Dir(dir))
	session, err := cmd.Run("create-react-app", name)
	if nil != err {
		return err
	}
	err = session.Wait()

	return
}

func (instance *DevUIControllerProjectCreator) createReactRedux(dir string) (err error) {
	name := lygo_paths.FileName(dir, false)
	cmd := environment.NewConsoleProgramWithDir("npx", lygo_paths.Dir(dir))
	session, err := cmd.Run("create-react-app", name, "--template redux") // npx create-react-app my-app --template redux
	if nil != err {
		return err
	}
	err = session.Wait()

	return
}
