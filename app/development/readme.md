# Development Controller #

Guardian has an internal development environment.

Guardian Development is an optional module disabled by default.
To enable Guardian Development you should add a `development.debug.json` or `development.production.json` configuration file 
and enable it by setting "enabled" field to true.

