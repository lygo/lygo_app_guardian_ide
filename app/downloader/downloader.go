package downloader

import (
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_sys"
	"errors"
	"os"
	"strings"
)

const PathWebUI = "https://bitbucket.org/lygo/lygo_app_guardian_ide/raw/master/_build/www/a-webui.zip"
const PathWebUIVersion = "https://bitbucket.org/lygo/lygo_app_guardian_ide/raw/master/_build/build_version.txt"
const PathRuntimeVersion = "https://bitbucket.org/lygo/lygo_app_guardian/raw/master/_build/build_version.txt"
const PathWindows = "https://bitbucket.org/lygo/lygo_app_guardian/raw/master/_build/windows/guardian.exe"
const PathLinux = "https://bitbucket.org/lygo/lygo_app_guardian/raw/master/_build/linux/guardian"
const PathMac = "https://bitbucket.org/lygo/lygo_app_guardian/raw/master/_build/mac/guardian"

//----------------------------------------------------------------------------------------------------------------------
//	t y p e
//----------------------------------------------------------------------------------------------------------------------

type Downloader struct {
	dirApp  string
	dirWork string
	dirDev  string

	pathWebUIVersion string
	pathExecVersion  string
	pathExec         string
	urlExec          string
}

func NewDownloader(dirApp, dirWork, dirDev string) *Downloader {
	instance := new(Downloader)
	instance.dirApp = dirApp
	instance.dirWork = dirWork
	instance.dirDev = dirDev

	instance.urlExec = PathMac
	if lygo_sys.IsLinux() {
		instance.urlExec = PathLinux
	} else if lygo_sys.IsWindows() {
		instance.urlExec = PathWindows
	}
	name := lygo_paths.FileName(instance.urlExec, true)
	instance.pathExec = lygo_paths.Concat(instance.dirApp, name)
	instance.pathExecVersion = lygo_paths.Concat(instance.dirApp, "version-rt.txt")
	instance.pathWebUIVersion = lygo_paths.Concat(instance.dirApp, "version-ui.txt")

	instance.init()

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Downloader) PathExec() string {
	return instance.pathExec
}

func (instance *Downloader) CurrentRuntimeVersion() string {
	v, _ := lygo_io.ReadTextFromFile(instance.pathExecVersion)
	return v
}

func (instance *Downloader) LatestRuntimeVersion() string {
	if nil != instance {
		data, err := lygo_io.Download(PathRuntimeVersion)
		if nil == err {
			return string(data)
		}
	}
	return "0.0.0"
}

func (instance *Downloader) CurrentWebUIVersion() string {
	v, _ := lygo_io.ReadTextFromFile(instance.pathWebUIVersion)
	return v
}

func (instance *Downloader) LatestWebUIVersion() string {
	if nil != instance {
		data, err := lygo_io.Download(PathWebUIVersion)
		if nil == err {
			return string(data)
		}
	}
	return "0.0.0"
}

func (instance *Downloader) AssertRuntimeLatest() error {
	if nil != instance {
		currentVersion := instance.CurrentRuntimeVersion()
		latestVersion := instance.LatestRuntimeVersion()
		if currentVersion != latestVersion {
			err := instance.downloadRuntimeVersion()
			if nil == err {
				err = instance.DownloadRuntime()
				if nil != err {
					// remove all
					_ = lygo_io.Remove(instance.pathExecVersion)
					_ = lygo_io.Remove(instance.pathExec)
				}
			}
			return err
		}
	}
	return nil
}

func (instance *Downloader) AssertWebUILatest() error {
	if nil != instance {
		currentVersion := instance.CurrentWebUIVersion()
		latestVersion := instance.LatestWebUIVersion()
		if currentVersion != latestVersion {
			err := instance.downloadWebUIVersion()
			if nil == err {
				err = instance.DownloadWebUI()
				if nil != err {
					// remove all
					_ = lygo_io.Remove(instance.pathWebUIVersion)
				}
			}
			return err
		}
	}
	return nil
}

func (instance *Downloader) AssertRuntimeExists() error {
	if nil != instance {
		if b, _ := lygo_paths.Exists(instance.pathExec); !b {
			return instance.DownloadRuntime()
		}
	}
	return nil
}

func (instance *Downloader) DownloadRuntime() error {
	if nil != instance {
		bytes, err := lygo_io.Download(instance.urlExec)
		if nil != err {
			return err
		}
		if !isValidBinary(bytes) {
			return lygo_errors.Prefix(errors.New(instance.urlExec), "File not found:")
		}

		_, err = lygo_io.WriteBytesToFile(bytes, instance.pathExec)
		if nil == err {
			// exec permissions
			err = os.Chmod(instance.pathExec, 0700)
		}
		return err
	}
	return nil
}

func (instance *Downloader) DownloadWebUI() error {
	if nil != instance {
		bytes, err := lygo_io.Download(PathWebUI)
		if nil != err {
			return err
		}
		if !isValidBinary(bytes) {
			return lygo_errors.Prefix(errors.New(PathWebUI), "File not found:")
		}
		target := lygo_paths.Concat(instance.dirDev, "a-webui.zip")
		_, err = lygo_io.WriteBytesToFile(bytes, target)
		if nil == err {
			// remove target
			defer lygo_io.Remove(target)

			// UNZIP
			zipTarget := lygo_paths.Concat(instance.dirDev, "_webui", "www")
			_ = lygo_io.RemoveAll(zipTarget)
			_ = lygo_paths.Mkdir(lygo_paths.ConcatDir(zipTarget))

			files, err := lygo_io.Unzip(target, zipTarget)
			if nil == err {
				for _, file := range files {
					// copy to www
					tf := strings.Replace(file, "build" + lygo_paths.OS_PATH_SEPARATOR, "", 1)
					_, _ = lygo_io.CopyFile(file, tf)
				}
				_ = lygo_io.RemoveAll(lygo_paths.Concat(zipTarget, "build"))
			}
		}
		return err
	}
	return nil
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Downloader) init() {
	if b, _ := lygo_paths.Exists(instance.pathExecVersion); !b {
		// _ = instance.downloadRuntimeVersion()
	}
	if b, _ := lygo_paths.Exists(instance.pathWebUIVersion); !b {
		// _ = instance.downloadWebUIVersion()
	}
}

func (instance *Downloader) downloadRuntimeVersion() error {
	if nil != instance {
		bytes, err := lygo_io.Download(PathRuntimeVersion)
		if nil != err {
			return err
		}
		_, err = lygo_io.WriteBytesToFile(bytes, instance.pathExecVersion)
		return err
	}
	return nil
}

func (instance *Downloader) downloadWebUIVersion() error {
	if nil != instance {
		bytes, err := lygo_io.Download(PathWebUIVersion)
		if nil != err {
			return err
		}
		_, err = lygo_io.WriteBytesToFile(bytes, instance.pathWebUIVersion)
		return err
	}
	return nil
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func isValidBinary(data []byte) bool {
	if len(data) > 0 {
		text := string(data)
		if strings.Index(text, "<") != 0 {
			return true
		}
	}
	return false
}
