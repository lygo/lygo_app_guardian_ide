package http

import (
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/commons"
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
)

type WebServerSettingsRouting struct {
	Method   string          `json:"method"`
	Endpoint string          `json:"endpoint"`
	Action   *commons.Action `json:"action"`
}

type WebServerSettings struct {
	Enabled       bool                                 `json:"enabled"`
	Http          map[string]interface{}               `json:"http"`
	Initialize    []*commons.Action                    `json:"initialize"` // initialization scripts
	Routing       []*WebServerSettingsRouting          `json:"routing"`
	Authorization *commons.ActionVariableAuthorization `json:"authorization"`
}

func (instance *WebServerSettings) Map() map[string]interface{} {
	return lygo_conv.ToMap(instance)
}
