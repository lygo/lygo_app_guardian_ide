package rt

import (
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/commons"
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/development/deverrors"
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/downloader"
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"bitbucket.org/lygo/lygo_commons/lygo_exec"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"bitbucket.org/lygo/lygo_events"
	"errors"
	"fmt"
	"strings"
	"sync"
)

const mode = "debug"

//----------------------------------------------------------------------------------------------------------------------
//	t y p e
//----------------------------------------------------------------------------------------------------------------------

type GuardianRuntime struct {
	running bool
	dirApp  string
	dirWork string
	logger  *commons.Logger
	events  *lygo_events.Emitter

	downloader *downloader.Downloader
	pathExec   string
	executor   *lygo_exec.Executor
	mux        sync.Mutex
}

func NewGuardianRuntime(dirApp, dirWork, dirDevFiles string, l *commons.Logger, e *lygo_events.Emitter) *GuardianRuntime {
	instance := new(GuardianRuntime)
	instance.running = false
	instance.dirApp = dirApp
	instance.dirWork = dirWork
	instance.logger = l
	instance.events = e

	instance.downloader = downloader.NewDownloader(dirApp, dirWork, dirDevFiles)
	instance.pathExec = instance.downloader.PathExec()

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *GuardianRuntime) Start() (err error) {
	if nil != instance {
		// async download newest runtime
		go func() {
			err := instance.downloader.AssertRuntimeLatest()
			if nil != err {
				instance.logger.Error("Error Updating Runtime of Guardian executor: ", err)
			}
			instance.startRuntime()
		}()

		err = instance.downloader.AssertWebUILatest()
	}
	return
}

func (instance *GuardianRuntime) Stop() (err error) {
	if nil != instance {
		instance.stopRuntime()
	}
	return err
}

func (instance *GuardianRuntime) Restart() (err error) {
	if nil != instance {
		instance.stopRuntime()
		instance.startRuntime()
	}
	return err
}

func (instance *GuardianRuntime) IsRunning() bool {
	if nil != instance {
		return instance.running
	}
	return false
}

func (instance *GuardianRuntime) CurrentVersion() string {
	if nil != instance {
		return instance.downloader.CurrentRuntimeVersion()
	}
	return ""
}

func (instance *GuardianRuntime) LatestVersion() string {
	if nil != instance {
		return instance.downloader.LatestRuntimeVersion()
	}
	return ""
}

func (instance *GuardianRuntime) LatestDownload() error {
	if nil != instance {
		return instance.downloader.DownloadRuntime()
	}
	return nil
}

func (instance *GuardianRuntime) LatestUiVersion() string {
	if nil != instance {
		return instance.downloader.LatestWebUIVersion()
	}
	return ""
}

func (instance *GuardianRuntime) CurrentUiVersion() string {
	if nil != instance {
		return instance.downloader.CurrentWebUIVersion()
	}
	return ""
}

func (instance *GuardianRuntime) LatestUiDownload() error {
	if nil != instance {
		return instance.downloader.DownloadWebUI()
	}
	return nil
}

func (instance *GuardianRuntime) Pid() int {
	if nil != instance && nil != instance.executor {
		return instance.executor.PidCurrent()
	}
	return -1
}

func (instance *GuardianRuntime) GetLog() (string, error) {
	if nil != instance {
		filename := instance.logger.GetFileName()
		text, err := lygo_io.ReadTextFromFile(filename)
		return text, err
	}
	return "", nil
}

func (instance *GuardianRuntime) OpenLogFile() error {
	if nil != instance {
		filename := instance.logger.GetFileName()
		return lygo_exec.Open(filename)
	}
	return nil
}

func (instance *GuardianRuntime) OpenLogDir() error {
	if nil != instance {
		filename := instance.logger.GetFileName()
		return lygo_exec.Open(lygo_paths.Dir(filename))
	}
	return nil
}

func (instance *GuardianRuntime) ClearLogFile() error {
	if nil != instance {
		filename := instance.logger.GetFileName()
		_, err := lygo_io.WriteTextToFile("", filename)
		return err
	}
	return nil
}

func (instance *GuardianRuntime) GetRuntimeLog() (string, error) {
	if nil != instance {
		filename := strings.ReplaceAll(instance.logger.GetFileName(), commons.DirDevelopment+lygo_paths.OS_PATH_SEPARATOR, "")
		text, err := lygo_io.ReadTextFromFile(filename)
		return text, err
	}
	return "", nil
}

func (instance *GuardianRuntime) OpenRuntimeLogFile() error {
	if nil != instance {
		filename := strings.ReplaceAll(instance.logger.GetFileName(), commons.DirDevelopment+lygo_paths.OS_PATH_SEPARATOR, "")
		return lygo_exec.Open(filename)
	}
	return nil
}

func (instance *GuardianRuntime) ClearRuntimeLogFile() error {
	if nil != instance {
		filename := strings.ReplaceAll(instance.logger.GetFileName(), commons.DirDevelopment+lygo_paths.OS_PATH_SEPARATOR, "")
		_, err := lygo_io.WriteTextToFile("", filename)
		return err
	}
	return nil
}

func (instance *GuardianRuntime) OpenRuntimeLogDir() error {
	if nil != instance {
		filename := strings.ReplaceAll(instance.logger.GetFileName(), commons.DirDevelopment+lygo_paths.OS_PATH_SEPARATOR, "")
		return lygo_exec.Open(lygo_paths.Dir(filename))
	}
	return nil
}

func (instance *GuardianRuntime) GetConfiguration() (map[string]interface{}, error) {
	if nil != instance {
		filename := lygo_paths.Concat(instance.dirWork, fmt.Sprintf("webserver.%v.json", mode))
		if b, _ := lygo_paths.Exists(filename); !b {
			return nil, lygo_errors.Prefix(errors.New(filename), deverrors.FileNotFoundDevError.Error())
		}
		m, err := lygo_json.ReadMapFromFile(filename)
		if nil != err {
			return nil, err
		}
		return m, nil
	}
	return nil, nil
}

func (instance *GuardianRuntime) GetStaticWWWRoot() string {
	config, err := instance.GetConfiguration()
	if nil!=err{
		return ""
	}
	chttp := lygo_reflect.Get(config, "http")
	if nil!=chttp{
		root := lygo_paths.Concat(instance.dirWork, lygo_reflect.GetString(chttp, "root"))
		cstatic := lygo_reflect.GetArray(chttp, "static")
		if len(cstatic)>0{
			www := lygo_reflect.GetString(cstatic[0], "root")
			return lygo_paths.Concat(root, www)
		}
	}
	return ""
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *GuardianRuntime) startRuntime() {
	if nil != instance {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		// ensure stopped
		if nil != instance.executor {
			_ = instance.executor.Kill()
			instance.executor = nil
		}

		instance.executor = lygo_exec.NewExecutorWithDir(instance.pathExec, lygo_paths.Dir(instance.pathExec))
		err := instance.executor.Run("run", "-m", mode, "-dir_work", instance.dirWork, "-dir_app", lygo_paths.Dir(instance.pathExec))
		if nil == err {
			pid := instance.executor.PidCurrent()
			instance.logger.Info(fmt.Sprintf("Guardian Executable=%v", instance.pathExec))
			instance.logger.Info(fmt.Sprintf("Guardian Process PID=%v", pid))
			instance.running = true
			instance.events.EmitAsync(commons.EventOnRuntimeStart)
		} else {
			instance.logger.Error("GuardianRuntime.startRuntime:", err)
			_ = instance.executor.Kill()
			instance.executor = nil
		}
	}
}

func (instance *GuardianRuntime) stopRuntime() {
	if nil != instance && nil != instance.executor {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		_ = instance.executor.Kill()
		instance.executor = nil
		instance.running = false
		instance.events.EmitAsync(commons.EventOnRuntimeStop)
	}
}
