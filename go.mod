module bitbucket.org/lygo/lygo_app_guardian_ide

go 1.15

require (
	bitbucket.org/lygo/lygo_commons v0.1.80
	bitbucket.org/lygo/lygo_events v0.1.5
	bitbucket.org/lygo/lygo_ext_auth0 v0.1.16
	bitbucket.org/lygo/lygo_ext_http v0.1.19
	bitbucket.org/lygo/lygo_ext_logs v0.1.6
	github.com/Djarvur/go-err113 v0.1.0 // indirect
	github.com/cbroglie/mustache v1.2.0
	github.com/chavacava/garif v0.0.0-20210405164556-e8a0a408d6af // indirect
	github.com/gofiber/fiber/v2 v2.7.1
	github.com/golangci/golangci-lint v1.39.0 // indirect
	github.com/golangci/misspell v0.3.5 // indirect
	github.com/google/go-cmp v0.5.5 // indirect
	github.com/gostaticanalysis/analysisutil v0.7.1 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/jirfag/go-printf-func-name v0.0.0-20200119135958-7558a9eaa5af // indirect
	github.com/julz/importas v0.0.0-20210405141620-a22c8f743dc9 // indirect
	github.com/magiconair/properties v1.8.5 // indirect
	github.com/mattn/go-runewidth v0.0.12 // indirect
	github.com/mgechev/revive v1.0.6 // indirect
	github.com/mitchellh/mapstructure v1.4.1 // indirect
	github.com/pelletier/go-toml v1.9.0 // indirect
	github.com/quasilyte/go-ruleguard v0.3.3 // indirect
	github.com/quasilyte/regex/syntax v0.0.0-20200805063351-8f842688393c // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/spf13/afero v1.6.0 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/tdakkota/asciicheck v0.0.0-20200416200610-e657995f937b // indirect
	github.com/timakin/bodyclose v0.0.0-20200424151742-cb6215831a94 // indirect
	github.com/tomarrell/wrapcheck v1.1.0 // indirect
	github.com/tommy-muehle/go-mnd v1.3.1-0.20201008215730-16041ac3fe65 // indirect
	golang.org/x/mod v0.4.2 // indirect
	golang.org/x/sys v0.0.0-20210403161142-5e06dd20ab57 // indirect
	golang.org/x/text v0.3.6 // indirect
	gopkg.in/ini.v1 v1.62.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	mvdan.cc/unparam v0.0.0-20210104141923-aac4ce9116a7 // indirect
)
