package main

import (
	"bitbucket.org/lygo/lygo_app_guardian_ide/app"
	"bitbucket.org/lygo/lygo_app_guardian_ide/app/commons"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"flag"
	"fmt"
	"os"
	"time"
)

var logger *commons.Logger

//----------------------------------------------------------------------------------------------------------------------
//	l a u n c h e r
//----------------------------------------------------------------------------------------------------------------------

func main() {
	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			message := lygo_strings.Format("[panic] APPLICATION %s ERROR: %s", commons.Name, r)
			if nil != logger {
				logger.Error(message)
				time.Sleep(3 * time.Second) // wait logger can write
			} else {
				fmt.Println(message)
			}
		}
	}()

	//-- command flags --//
	// run
	cmdRun := flag.NewFlagSet("run", flag.ExitOnError)
	dirStart := cmdRun.String("dir_start", lygo_paths.Absolute("./"), "Set a particular folder as start directory")
	dirApp := cmdRun.String("dir_app", lygo_paths.Absolute("./"), "Set a particular folder as binary directory")
	dirWork := cmdRun.String("dir_work", lygo_paths.Absolute("./_guardian"), "Set a particular folder as main workspace")
	mode := cmdRun.String("m", commons.ModeDebug, "Mode allowed: 'debug' or 'production'")

	// parse
	if len(os.Args) > 1 {
		cmd := os.Args[1]
		switch cmd {
		case "run":
			_ = cmdRun.Parse(os.Args[2:])
		}
	}

	// init
	lygo_paths.GetWorkspace(commons.WpDirStart).SetPath(*dirStart)
	lygo_paths.GetWorkspace(commons.WpDirApp).SetPath(*dirApp)
	lygo_paths.GetWorkspace(commons.WpDirWork).SetPath(*dirWork)

	// initialize global variables
	commons.InitPaths()

	logger = commons.NewLogger(*mode)
	logger.Info(lygo_strings.Format("APPLICATION %s v.%s mode '%s'", commons.Name, commons.Version, *mode))
	logger.Info(lygo_strings.Format("DIR_START: %s", lygo_paths.GetWorkspace(commons.WpDirStart).GetPath()))
	logger.Info(lygo_strings.Format("DIR_APP: %s", lygo_paths.GetWorkspace(commons.WpDirApp).GetPath()))
	logger.Info(lygo_strings.Format("DIR_WORK: %s", lygo_paths.GetWorkspace(commons.WpDirWork).GetPath()))

	application := app.NewApp(*mode, logger)
	err := application.Start()
	if nil != err {
		panic(err)
	}
	application.Wait() // lock waiting guardian exit or close gracefully

	fmt.Println(fmt.Sprintf("Quit %s", commons.Name))
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

